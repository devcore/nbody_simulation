/*
File: NBodyLayouts.cpp
--------------------------------------------------------------------------------
For details see NBodyLayouts.h
*/
#include "NBodyLayouts.h"

using namespace glm;

void Layouts::SpiralGalaxy(std::vector<NBody> &nbodies, uint size)
{
    nbodies.resize(size);
    // Initialise defaults
    vec3 pos[4]{ vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f) };
    vec3 vel[4]{ vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f) };
    float mass[4]
    { 
        200000000000.0f,
        200000000000.0f,
        200000000000.0f,
        200000000000.0f
    };
    mat4 spin = rotate(mat4(1.0f), half_pi<float>(), vec3(0.0f, 1.0f, 0.0f));
    vec3 p = vec3(1.0f);
    // Form a spiral galaxy of NBodies
    uint x4{ 0 };
    for (uint i = 0; i < nbodies.size(); ++i)
    {
        for (uint j = 0; j < 4; ++j, ++x4)
        {
            float p = static_cast<float>(x4 + 10);
            float scale = 2.0f - p / nbodies.size()*2.5f;
            pos[j] = vec3(sinf(p)*p*0.15f, sinf(p*p)*p*0.01f + 0.01f, cosf(p)*p*0.15f);
            // Add spin to the galaxy
            vel[j] = normalize(vec3(vec4(-pos[j], 1.0f)*spin))*scale;
        }
        nbodies[i] = NBody(pos, vel, mass);
    }
}

void Layouts::TwinSpiralGalaxies(std::vector<NBody> &nbodies, glm::uint size)
{
    nbodies.resize(size);
    // Initialise defaults
    vec3 pos[4]{ vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f) };
    vec3 vel[4]{ vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f) };
    vec3 offset = vec3(static_cast<float>(std::cbrt(size*size)));
    mat4 spin = rotate(mat4(1.0f), quarter_pi<float>(), vec3(0.0f, 1.0f, 0.0f));
    float mass[4]
    { 
        200000000000.0f,
        200000000000.0f,
        200000000000.0f,
        200000000000.0f
    };
    uint n{ 0 }, x4{ 0 };
    // Form 1st spiral galaxy of NBodies with an positive offset
    for (uint i = 0; i < nbodies.size()/2; ++i, ++n)
    {
        for (uint j = 0; j < 4; ++j, ++x4)
        {
            float p = static_cast<float>(x4 + 16);
            float scale = 2.0f - p / nbodies.size()*4.0f;
            pos[j] = vec3(sinf(p)*p*0.15f, sinf(p*p)*p*0.04f + 0.01f, cosf(p)*p*0.15f);
            pos[j] += offset;
            // Add spin to the galaxy
            vel[j] = -normalize(vec3(vec4(offset - pos[j], 1.0f)*spin))*scale;
            vel[j].y -= 5.0f;
        }
        nbodies[n] = NBody(pos, vel, mass);
    }
    x4 = 0;
    // Form 2nd spiral galaxy of NBodies with an negative offset
    for (uint i = 0; i < nbodies.size()/2; ++i, ++n)
    {
        for (uint j = 0; j < 4; ++j, ++x4)
        {
            float p = static_cast<float>(x4 + 16);
            float scale = 2.0f - p / nbodies.size()*4.0f;
            pos[j] = vec3(sinf(p)*p*0.15f, sinf(p*p)*p*0.04f + 0.01f, cosf(p)*p*0.15f);
            pos[j] -= offset;
            // Add spin to the galaxy
            vel[j] = normalize(vec3(vec4(offset + pos[j], 1.0f)*spin))*scale;
            vel[j].y += 5.0f;
        }
        nbodies[n] = NBody(pos, vel, mass);
    }
}

void Layouts::CubeDistribution(std::vector<NBody> &nbodies, glm::uint size)
{
    nbodies.resize(size);
    uint croot_size{ (uint)std::floorf(std::cbrt(static_cast<float>(size))) };
    // Initiliase default values
    vec3 pos[4]{ vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f) };
    float base_mass{ 200000000000.0f };
    float mass[4]{ base_mass, base_mass, base_mass, base_mass };
    float offset{ 2.0f };
    vec3 offsets[4] 
    {
      vec3( static_cast<float>(croot_size), 0.0f,  static_cast<float>(croot_size)),
      vec3(-static_cast<float>(croot_size), 0.0f,  static_cast<float>(croot_size)),
      vec3(-static_cast<float>(croot_size), 0.0f, -static_cast<float>(croot_size)),
      vec3( static_cast<float>(croot_size), 0.0f, -static_cast<float>(croot_size))
    };
    // Make a cuboid from points
    uint n{ 0 }, i{ 0 }, j{ 0 }, k{ 0 };
    for (i = 0; i < croot_size; ++i)
    for (j = 0; j < croot_size; ++j)
    for (k = 0; k < croot_size; ++k, ++n)
    {
        pos[0] = vec3( offset*i, offset*j,  offset*k) + offsets[0];
        pos[1] = vec3(-offset*i, offset*j,  offset*k) + offsets[1];
        pos[2] = vec3(-offset*i, offset*j, -offset*k) + offsets[2];
        pos[3] = vec3( offset*i, offset*j, -offset*k) + offsets[3];
        nbodies[n] = NBody(pos, mass);
    }
    for (j = croot_size; n < size; j++)
    for (i = 0; i < croot_size && n < size; i++)
    for (k = 0; k < croot_size && n < size; k++, n++)
    {
        pos[0] = vec3( offset*i, offset*j,  offset*k) + offsets[0];
        pos[1] = vec3(-offset*i, offset*j,  offset*k) + offsets[1];
        pos[2] = vec3(-offset*i, offset*j, -offset*k) + offsets[2];
        pos[3] = vec3( offset*i, offset*j, -offset*k) + offsets[3];
        nbodies[n] = NBody(pos, mass);
    }
}

void Layouts::RandomDistribution(std::vector<NBody> &nbodies, glm::uint size)
{
    nbodies.resize(size);
    // Default values
    vec3 pos[4]{ vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f) };
    float base_mass{ 200000000000.0f };
    float mass[4]{ base_mass, base_mass, base_mass, base_mass };
    float offset{ static_cast<float>(size)*0.2f };
    // Create random number generator
    std::default_random_engine gen;
    std::uniform_real<float> dist(-offset, offset);
    std::uniform_real<float> dist_mass(base_mass, base_mass*2.0f);
    // Distribute NBodies randomly
    uint x4{ 0 };
    for (uint i = 0; i < size; ++i)
    {
        for (uint j = 0; j < 4; ++j, ++x4)
        {
            pos[j] = vec3(dist(gen), dist(gen), dist(gen));
            mass[j] = dist_mass(gen);
            while (glm::length2(pos[j]) > offset*offset)
            {
                pos[j] = vec3(dist(gen), dist(gen), dist(gen));
            }
        }
        nbodies[i] = NBody(pos, mass);
    }
}

void Layouts::Singularity(std::vector<NBody> &nbodies, glm::uint size)
{
    nbodies.resize(size);
    // Default values
    vec3 pos[4]{ vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f) };
    float base_mass{ 200000000000.0f };
    float mass[4]{ base_mass, base_mass, base_mass, base_mass };
    float offset{ static_cast<float>(size)*0.016f };
    // Create random number generator
    std::default_random_engine gen;
    std::uniform_real<float> dist_pos(-offset, offset);
    std::uniform_real<float> dist_mass(base_mass, base_mass*2.0f);
    // Distribute NBodies randomly within a tight sphere
    uint x4{ 0 };
    for (uint i = 0; i < size; ++i)
    {
        for (uint j = 0; j < 4; ++j, ++x4)
        {
            pos[j] = vec3(dist_pos(gen), dist_pos(gen), dist_pos(gen));
            mass[j] = dist_mass(gen);
            while (glm::length2(pos[j]) > offset*offset)
            {
                pos[j] = vec3(dist_pos(gen), dist_pos(gen), dist_pos(gen));
            }
        }
        nbodies[i] = NBody(pos, mass);
    }
}

// Layouts 8 NBodies diagonally apart
void Layouts::TestLayout(std::vector<NBody> &nbodies, uint size)
{
    nbodies.resize(size);
    // Setup corner positions
    vec3 positions[4]
    {
        vec3(10.0f, 10.0f, 10.0f),
        vec3(10.0f, 10.0f, -10.0f),
        vec3(-10.0f, 10.0f, -10.0f),
        vec3(-10.0f, 10.0f, 10.0f),
    };
    float mass[4]
    {
        100000000000.0f,
        100000000000.0f,
        100000000000.0f,
        100000000000.0f
    };
    // Place two bodies at a time: top and bottom
    nbodies[0] = NBody(positions, mass);
    for (uint i = 0; i < 4; i++)
    {
        positions[i] = -positions[i];
    }
    nbodies[1] = NBody(positions, mass);
}