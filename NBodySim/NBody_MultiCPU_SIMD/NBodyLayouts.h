/*--------------------------------------------------------\
| File: NBodyLayout.h                                     |
|x-------------------------------------------------------x|
| Details: Positions NBodies in various foramations:      |
| spiral, twin spiral, cubiod galaxies etc.               |
|x-------------------------------------------------------x|
| Author:                              Andrius Jefremovas |
\--------------------------------------------------------*/
#ifndef NBODY_LAYOUT_H
#define NBODY_LAYOUT_H

#include "NBodySystem.h"

namespace Layouts
{
    // Layouts NBodies in a spiral galaxy distribution
    void SpiralGalaxy(std::vector<NBody> &nbodies, glm::uint size);
    // Layouts NBodies into two spiral galaxies
    void TwinSpiralGalaxies(std::vector<NBody> &nbodies, glm::uint size);
    // Layouts NBodies in a approximate cube given any number
    void CubeDistribution(std::vector<NBody> &nbodies, glm::uint size);
    // Layouts NBodies using random distribution
    void RandomDistribution(std::vector<NBody> &nbodies, glm::uint size);
    // Layouts NBodies randomly in a sphere with velocity towards the center
    void Singularity(std::vector<NBody> &nbodies, glm::uint size);
    // Layouts 8 NBodies diagonally apart
    void TestLayout(std::vector<NBody> &nbodies, glm::uint size);
};

#endif/* !NBODY_LAYOUT_H */