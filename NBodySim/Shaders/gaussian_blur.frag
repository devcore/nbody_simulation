#version 430

uniform sampler2D tex;   // Incoming frame data
uniform vec2 blur_step;  // Horizontal or vertical pass
uniform vec2 resolution; // Texel calculation

/* Incoming */
layout (location = 0) in vec2 tex_coord;

/* Outgoing */
layout (location = 0) out vec4 colour; 

// Texel weights
const float weights[9] = 
{
    0.1362162162f, // 2x Lower since loop computes 0 and -0
    0.1945945946f,
    0.1216216216f,
    0.0540540541f,
    0.0540540541f,
    0.0162162162f,
    0.0162162162f,
    0.0081081081f,
    0.0040540541f
};

void main()
{
    // Start with colour as black
    vec4 local_colour = vec4(0.0f);
    vec2 offset = 1.0f/resolution*blur_step;
    // Increase colour intensity using a weight and texel offset
    for (int i = 0; i < 9; ++i)
    {
        local_colour += texture(tex, tex_coord+i*offset) * weights[i];
        local_colour += texture(tex, tex_coord-i*offset) * weights[i];
    }
    // Output colour
    colour = local_colour;
}