#version 440

// Incoming frame data
uniform sampler2D tex;

// 1.0f / screen width
uniform float inverse_width = 1.0f/1280.0f;
// 1.0f / screen height
uniform float inverse_height = 1.0f/720.0f;

uniform vec2 blur_step;

// Surrounding pixels to sample and their scale
const vec4 samples[9] = vec4[9]
(
    vec4( 0.0f,  0.0f, 0.0f, 0.105f),
    vec4( 0.0f, -1.0f, 0.0f, 0.125f),
    vec4(-1.0f,  0.0f, 0.0f, 0.125f),
    vec4( 1.0f,  0.0f, 0.0f, 0.125f),
    vec4( 0.0f,  1.0f, 0.0f, 0.125f),
    vec4( 1.0f,  1.0f, 0.0f, 0.075f),
    vec4(-1.0f,  1.0f, 0.0f, 0.075f),
    vec4(-1.0f, -1.0f, 0.0f, 0.075f),
    vec4( 1.0f, -1.0f, 0.0f, 0.075f)
);

// Incoming texture coordinate
layout (location = 0) in vec2 tex_coord;

// Outgoing colour
layout (location = 0) out vec4 colour;

void main()
{
    // Start with colour as black
    vec4 col = vec4(0.0f, 0.0f, 0.0f, 0.0f);
    vec2 radius = vec2(inverse_width, inverse_height);
    
    col += texture(tex, tex_coord-5.0f*radius*blur_step) * 0.0162162162f;
    col += texture(tex, tex_coord-4.0f*radius*blur_step) * 0.0162162162f;
    col += texture(tex, tex_coord-3.0f*radius*blur_step) * 0.0540540541f;
    col += texture(tex, tex_coord-2.0f*radius*blur_step) * 0.1216216216f;
    col += texture(tex, tex_coord-1.0f*radius*blur_step) * 0.1945945946f;
    
    col += texture(tex, tex_coord) * 0.23f;//0.2270270270f;
    
    col += texture(tex, tex_coord+1.0f*radius*blur_step) * 0.1945945946f;
    col += texture(tex, tex_coord+2.0f*radius*blur_step) * 0.1216216216f;
    col += texture(tex, tex_coord+3.0f*radius*blur_step) * 0.0540540541f;
    col += texture(tex, tex_coord+4.0f*radius*blur_step) * 0.0162162162f;
    col += texture(tex, tex_coord+5.0f*radius*blur_step) * 0.0162162162f;

    
    // Loop through each sample vector
    //for (int i = 0; i < 9; ++i)
    //{
    //    // Calculate tex coord to sample
    //    vec2 uv = tex_coord + vec2(samples[i].x * inverse_width, samples[i].y * inverse_height) * blur_step;
    //    // Sample the texture and scale appropriately
    //    // - scale factor stored in w component
    //    col += texture(tex, uv) * samples[i].w;
    //}
    colour = col;
    colour.a = 1.0f;
    //colour = vec4(blur_step, 0.0f, 1.0f);
    //colour = mix(col, texture(tex, tex_coord), 1.0f);
    //colour = texture(tex, tex_coord);
}