#version 430

// Incoming frame data
uniform sampler2D tex;

// Incoming texture coordinate
layout (location = 0) in vec2 tex_coord;

// Outgoing colour
layout (location = 0) out vec4 colour;

void main()
{
    colour = texture(tex, tex_coord);
}