#version 430

/* Incomming */
layout (location = 0) in vec4 in_colour;
/* Outgoing */
layout (location = 0) out vec4 out_colour;

void main()
{
    out_colour = in_colour;
}