#version 430

uniform sampler2D current_frame;
uniform sampler2D previous_frame;
uniform float blend_factor = 0.7f;

/* Incoming */
layout (location = 0) in vec2 tex_coord; 
/* Outgoing */
layout (location = 0) out vec4 colour; 

void main()
{
    vec4 current_colour = texture(current_frame, tex_coord);
    vec4 previous_colour = texture(previous_frame, tex_coord)*blend_factor;

    colour = clamp(current_colour + previous_colour, previous_colour, current_colour);
    //colour = mix(current_colour, previous_colour, 0.9f);
}