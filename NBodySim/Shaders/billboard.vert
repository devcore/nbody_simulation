#version 430

/* Incomming */
uniform mat4 V;
uniform vec3 camera_pos = vec3(0.0f);
layout (location = 0) in vec4 in_position;

/* Outgoing */
layout (location = 0) out float out_scale;

void main()
{
    // Transform position into camera space
    gl_Position = V * vec4(in_position.xyz, 1.0f);
    // Calculate billboard scale increase based on distance from camera
    vec3 dist = abs((in_position.xyz-camera_pos)*0.00375f);
    out_scale = clamp(dist.x+dist.y+dist.z, 1.0f, 15.0f);
}