#version 430

/* Incomming */
uniform mat4 P;
layout (points) in;
layout (location = 0) in float in_scale[];

/* Outgoing */
layout (triangle_strip, max_vertices = 17) out;
layout (location = 0) out vec4 out_colour;

const vec3 octogon[8] = 
{
    vec3(   0.0f,    1.0f, 0.0f),
    vec3( 0.707f,  0.707f, 0.0f),
    vec3(   1.0f,    0.0f, 0.0f),
    vec3( 0.707f, -0.707f, 0.0f),
    vec3(   0.0f,   -1.0f, 0.0f),
    vec3(-0.707f, -0.707f, 0.0f),
    vec3(  -1.0f,    0.0f, 0.0f),
    vec3(-0.707f,  0.707f, 0.0f),
};

const vec4 white = {1.0f, 1.0f, 1.0f, 1.0f};
const vec4 orange = {1.0f, 0.5f, 0.0f, 1.0f};

void main()
{
    vec3 position = gl_in[0].gl_Position.xyz;
    
    // Emit the octogon billboard
    for (int i=0; i<8; ++i)
    {
        gl_Position = P * vec4(position+octogon[i]*in_scale[0], 1.0f);
        out_colour = orange;
        EmitVertex();
        gl_Position = P * vec4(position, 1.0f);
        out_colour = white;
        EmitVertex();
    }
    gl_Position = P * vec4(position+octogon[0]*in_scale[0], 1.0f);
    out_colour = orange;
    EmitVertex();

    EndPrimitive();
}






// QUAD
//vec3 quad[4] = 
//{
//    vec3( 0.5f,  0.0f, 0.0f),
//    vec3( 0.0f,  0.5f, 0.0f),
//    vec3( 0.0f, -0.5f, 0.0f),
//    vec3(-0.5f,  0.0f, 0.0f),
//};

// HEXAGON
//vec3 hexagon[6] = 
//{
//    vec3(   0.0f,  1.0f, 0.0f),
//    vec3(-0.866f,  0.5f, 0.0f),
//    vec3( 0.866f,  0.5f, 0.0f),
//    vec3(-0.866f, -0.5f, 0.0f),
//    vec3( 0.866f, -0.5f, 0.0f),
//    vec3(   0.0f, -1.0f, 0.0f),
//};