/*--------------------------------------------------------\
| File: NBodySystem.h                                     |
|x-------------------------------------------------------x|
| Details: Creates a number of NBodies that attract each  |
| other with O(n^2) computational complexity. Attraction  |
| is calculated using: a = G*M_j*R_ij / (|R_ij|^2+E)^3/2  |
| Where: a - acceleration, G - gravitational constant,    |
| M - mass, R - distance and E - smoothing factor.        |
| Formula from: NVidia GPU gems 3 NBody Simulation        |
|x-------------------------------------------------------x|
| Author:                              Andrius Jefremovas |
| Description:    Simulates NBody attraction between them.|
\--------------------------------------------------------*/
#ifndef NBODY_SYSTEM_H
#define NBODY_SYSTEM_H

#include <graphics_framework.h>

// Defination of a single NBody
struct NBody
{
    float     mass;     // Mass         |  4 |
    glm::vec3 pos;      // Position     | 16 |
    glm::vec3 vel;      // Velocity     | 28 |
    glm::vec3 accel;    // Acceleration | 40 |
    // Default constructor
    NBody() : pos(0.0f), vel(0.0f), accel(0.0f), mass(1.0f) { }
    // Creates stationary NBody: position, mass.
    NBody(const glm::vec3 &pos, const float mass) :
        pos(pos), vel(0.0f), accel(0.0f), mass(mass) { }
    // Creates moving NBody: position, velocity, mass.
    NBody(const glm::vec3 &pos, const glm::vec3 &vel, const float mass) :
        pos(pos), vel(vel), accel(0.0f), mass(mass) { }
};

struct NBodySystem
{
    NBodySystem();
    ~NBodySystem();
    // Resets nbodies to new locations and resizes OpenGL buffer
    void ResetToNewLayout(const glm::uint size,
         void(*pFunc)(std::vector<NBody> &nbodies, const glm::uint size));
        //const std::function<void(std::vector<NBody> &nb, glm::uint size)> &func);
    // Takes input to control simulation and calls integrate/streambuffer
    void Update(const float delta_time);
    // Renders each NBody using the buffer of positions to create quads
    void Render(const graphics_framework::camera *cam);
private:
    // Performs NBody attraction and new position calculations
    void Integrate(const float delta_time);
    // Streams NBody positions to a OpenGL buffer
    void StreamBuffer();
    // Test how many NBodies can be simulated withing 60 fps
    void NBodyLimitTest();
    // Test iteration timings of various numbers of nbodies
    void PerformanceTest();
    // Performs integration for N iterations and print the results to a file
    void TestIntegration(const glm::uint iter);
    /* System properties */
    bool m_running;
    bool m_limit_test;
    glm::uint m_num_nbodies;
    std::vector<NBody> m_nbodyBuffer;
    // NBody display properties
    graphics_framework::effect m_eff;
    graphics_framework::geometry m_quad;
};

#endif /* !NBODY_SYSTEM_H */