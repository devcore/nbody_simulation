/*
File: NBodySystem.cpp
--------------------------------------------------------------------------------
For details see NBodySystem.h
*/
#include "NBodySystem.h"
#include "NBodyLayouts.h"
#include "InputHandler.h"

using namespace glm;
using namespace graphics_framework;

// Constants
const float g_smoothing{ 0.8f };
const float g_gravitation{ 6.674f*powf(10.0f, -11) };
const float g_damping{ 0.99987f };
// Quick buffer macro in bytes
#define BUFFER_SIZE m_nbodyBuffer.size()*sizeof(vec3)

NBodySystem::NBodySystem()
{
    std::cerr << "NBODY: NBodySystem created." << std::endl;
    // Initialise default settings
    m_running = false;
    m_limit_test = false;
#if DEBUG | _DEBUG
    m_num_nbodies = 128;
#else
    m_num_nbodies = 256;
#endif
    m_nbodyBuffer.reserve(65536);
    // Load default cube distribution
    Layouts::SpiralGalaxy(m_nbodyBuffer, m_num_nbodies);
    std::vector<vec3> positions(m_nbodyBuffer.size());
    for (uint i = 0; i < m_nbodyBuffer.size(); ++i)
    {
        positions[i] = m_nbodyBuffer[i].pos;
    }
    // VBO and shader program
    m_quad.set_type(GL_POINTS);
    m_quad.add_buffer(positions, POSITION_BUFFER, GL_STREAM_DRAW);
    m_eff.add_shader("..\\Shaders\\billboard.vert", GL_VERTEX_SHADER);
    m_eff.add_shader("..\\Shaders\\billboard.geom", GL_GEOMETRY_SHADER);
    m_eff.add_shader("..\\Shaders\\billboard.frag", GL_FRAGMENT_SHADER);
    m_eff.build();
#if 0  // Performance test
    PerformanceTest();
#elif 0  // Unit test
    TestIntegration(1000);
#endif
    std::cerr << "NBODY: NBody count: " << m_nbodyBuffer.size() << std::endl;
}

NBodySystem::~NBodySystem()
{
    std::cerr << "NBODY: NBodySystem Destroyed." << std::endl;
    m_nbodyBuffer.clear();
}

void NBodySystem::ResetToNewLayout(
    const uint size, void(*pfunc)(std::vector<NBody> &nbodies, const uint size))
{
    m_running = false; // Pause the simulation
    // Mark current VBO data as invalid
    glBindBuffer(GL_ARRAY_BUFFER, m_quad.get_buffer(POSITION_BUFFER));
    glBufferData(GL_ARRAY_BUFFER, BUFFER_SIZE, nullptr, GL_STREAM_DRAW);
    m_nbodyBuffer.clear();               
    pfunc(m_nbodyBuffer, size); // Layout NBodies in new pattern
    // Reallocate new buffer with the new size and update it with new positions
    glBufferData(GL_ARRAY_BUFFER, BUFFER_SIZE, nullptr, GL_STREAM_DRAW);
    vec3* ptr_mapbuffer = (vec3*)glMapBufferRange(GL_ARRAY_BUFFER, 0, BUFFER_SIZE, 
                                    GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT);
    for (uint i = 0; i < m_nbodyBuffer.size(); ++i)
    {
        ptr_mapbuffer[i] = m_nbodyBuffer[i].pos;
    }
    glUnmapBuffer(GL_ARRAY_BUFFER);
    // Update geomtry vertex count
    m_quad.set_vertex_count(m_nbodyBuffer.size());
}

void NBodySystem::Update(const float delta_time)
{
    // Change NBody layouts
    if (KeyPressOnce('1')) { ResetToNewLayout(m_num_nbodies, Layouts::SpiralGalaxy); }
    if (KeyPressOnce('2')) { ResetToNewLayout(m_num_nbodies, Layouts::TwinSpiralGalaxies); }
    if (KeyPressOnce('3')) { ResetToNewLayout(m_num_nbodies, Layouts::CubeDistribution); }
    if (KeyPressOnce('4')) { ResetToNewLayout(m_num_nbodies, Layouts::RandomDistribution); }
    if (KeyPressOnce('5')) { ResetToNewLayout(m_num_nbodies, Layouts::Singularity); }
    // Max NBody routine
    if (KeyPressOnce('6')) { m_limit_test = true; }
    if (KeyPressOnce('T')) { TestIntegration(100); }
    if (m_limit_test) { NBodyLimitTest(); }
    // Pause/Unpause simulation
    if (KeyPressOnce(GLFW_KEY_SPACE)) { m_running = !m_running; }
    if (m_running)
    { 
        Integrate(delta_time);
        StreamBuffer();
    }
}

#pragma optimize( "", off )
void NBodySystem::Integrate(const float delta_time)
{
    // Compute NBody attraction to each other. a = G*M_j*R_ij/(|R_ij|^2+E)^3/2
    const uint nbody_count{ m_nbodyBuffer.size() };
    for (uint i = 0; i < nbody_count; ++i)
    for (uint j = 0; j < nbody_count; ++j)
    {
        // R_ij - Distance between bodies
        vec3 dist = m_nbodyBuffer[i].pos - m_nbodyBuffer[j].pos;
        // G*M_j*R_ij - Multiply Gravitational Constant, Mass and Distance 
        vec3 numerator = g_gravitation * m_nbodyBuffer[j].mass * dist;
        // Add Manitude and Smoothing length then raise to power 3/2
        float denominator = glm::pow(length2(dist) + g_smoothing, 3.0f/2.0f);
        // Accumulate acceleration
        m_nbodyBuffer[i].accel -= numerator / denominator;
    }
    // Eulerian semi-implicit integration
    for (uint i = 0; i < nbody_count; ++i)
    {
        m_nbodyBuffer[i].vel += m_nbodyBuffer[i].accel * delta_time;
        m_nbodyBuffer[i].pos += m_nbodyBuffer[i].vel * delta_time;
        m_nbodyBuffer[i].accel = vec3(0.0f);
        m_nbodyBuffer[i].vel *= g_damping;
    }
}
#pragma optimize( "", on )

void NBodySystem::StreamBuffer()
{
    // Stream new positions
    glBindBuffer(GL_ARRAY_BUFFER, m_quad.get_buffer(POSITION_BUFFER));
    // GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT | GL_MAP_INVALIDATE_RANGE_BIT
    vec3* ptr_mapbuffer = (vec3*)glMapBufferRange(GL_ARRAY_BUFFER, 0, BUFFER_SIZE,
                                 GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT);
    for (uint i = 0; i < m_nbodyBuffer.size(); ++i)
    {
        ptr_mapbuffer[i] = m_nbodyBuffer[i].pos;
    }
    glUnmapBuffer(GL_ARRAY_BUFFER);
}

void NBodySystem::Render(const camera *cam)
{
    // Render a quad billboard for each nbody point
    renderer::bind(m_eff);
    const mat4 V = cam->get_view();
    const mat4 P = cam->get_projection();
    const vec3 cam_pos = cam->get_position();
    glUniformMatrix4fv(m_eff.get_uniform_location("V"), 1, GL_FALSE, value_ptr(V));
    glUniformMatrix4fv(m_eff.get_uniform_location("P"), 1, GL_FALSE, value_ptr(P));
    glUniform3fv(m_eff.get_uniform_location("camera_pos"), 1, value_ptr(cam_pos));
    renderer::render(m_quad);
}

void NBodySystem::NBodyLimitTest()
{
    using namespace std::chrono;
    // Create timing functions
    auto start = high_resolution_clock::now();
    auto end = high_resolution_clock::now();
    float delta_time = 0.0f;
    const uint iter_count = 5;
    const float target_FPS = iter_count * 0.016f;
    // Loop while the frame time is ~60 times a second
    for (uint n = 0; n < iter_count && delta_time < target_FPS; ++n)
    {
        // Push back 4 new NBodies
        if (n != 0)
        {
            m_nbodyBuffer.push_back(NBody(vec3(0.0f), 1000000.0f));
            m_nbodyBuffer.push_back(NBody(vec3(0.0f), 1000000.0f));
            m_nbodyBuffer.push_back(NBody(vec3(0.0f), 1000000.0f));
            m_nbodyBuffer.push_back(NBody(vec3(0.0f), 1000000.0f));
        }
        // Start time stamp
        start = high_resolution_clock::now();
        for (uint i = 0; i < iter_count; ++i)
        {
            Integrate(delta_time);
        }
        // Compute time taken
        end = high_resolution_clock::now();
        delta_time = (float)(duration_cast<microseconds>(end-start).count());
        delta_time *= 0.000001f;
    }
    // If the exit was due to performance, cancel futher tests
    if (delta_time > target_FPS)
    {
        m_limit_test = false;
        m_num_nbodies = m_nbodyBuffer.size();
    }
    ResetToNewLayout(m_nbodyBuffer.size(), Layouts::CubeDistribution);
    std::cerr << "NBODY: New NBody count: " << m_nbodyBuffer.size() << std::endl;
}

void NBodySystem::PerformanceTest()
{
    using namespace std::chrono;
    // Pre-create variables used for timing and setup the system
    auto start = high_resolution_clock::now();
    auto end = high_resolution_clock::now();
    double total{ 0.0 };
    const uint iter_count = 1000;
    const uint nbody_count[4]{ 1048, 2048, 4096, 8192 };
    // Create a file to store the data
    std::ofstream data_file("NBody_performance.csv", std::ofstream::out);
    data_file << "NBodies, 1024, 2048, 4096, 8192\n";
    data_file << "Test_Name, ";
    // Test with 4 different sizes for 1000 iterations
    for (uint n = 0; n < 4; ++n)
    {
        Layouts::SpiralGalaxy(m_nbodyBuffer, nbody_count[n]);
        start = high_resolution_clock::now();
        for (uint i = 0; i < iter_count; ++i)
        {
            Integrate(0.02f);
        }
        end = high_resolution_clock::now();
        total = static_cast<double>(duration_cast<microseconds>(end - start).count());
        std::cerr << n << total << std::endl;
        total /= iter_count; // Compute average
        total *= 0.001; // Convert to milliseconds
        data_file << total << ", ";
        std::cerr << n << ": " << total << std::endl;
    }
    data_file.close();
}

void NBodySystem::TestIntegration(uint iter)
{
    using namespace std;
    // Space out 8 nbodies evenly
    m_num_nbodies = 8;
    ResetToNewLayout(m_num_nbodies, Layouts::TestLayout);
    // Integrate for N times
    for (uint i = 0; i < iter; ++i)
    {
        Integrate(0.01f);
    }
    // Store the new positions in a file
    string file_name = "SINGLE_CPU_IntegrationTest_"+to_string(iter)+string(".csv");
    ofstream data_file(file_name, ofstream::out);
    data_file << "Positions after: " << iter << "\n";
    data_file << "x,y,z" << "\n";
    for (uint i = 0; i < m_num_nbodies; i++)
    {
        data_file << m_nbodyBuffer[i].pos.x << ",";
        data_file << m_nbodyBuffer[i].pos.y << ",";
        data_file << m_nbodyBuffer[i].pos.z << ",";
        data_file << "\n";
    }
    data_file.close();
    StreamBuffer();
}