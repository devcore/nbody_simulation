/*
File: CameraSystem.cpp
--------------------------------------------------------------------------------
For details see CameraSystem.h
*/
#include "CameraSystem.h"
#include "InputHandler.h"
#include <FreeImage\FreeImage.h>

using namespace glm;
using namespace graphics_framework;

CameraSystem::CameraSystem()
{
    std::cerr << "NBODY: CameraSystem created." << std::endl;
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    m_lock_mouse = false;
    m_active_cam = CAMERA_TYPES::ARC_CAM;
    m_current_frame = 0;
    // Initialise cursor position at zero
    m_cursor_x = m_cursor_y = 0.0;
    m_delta_x = m_delta_y = 0.0;
    // Hide cursor arrow
    glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    // Get current position of cursor
    glfwGetCursorPos(renderer::get_window(), &m_cursor_x, &m_cursor_y);
    // Calculate aspect and screen ratios
    m_aspect = static_cast<float>(renderer::get_screen_width()) /
        static_cast<float>(renderer::get_screen_height());
    m_ratio_width = quarter_pi<float>() /
        static_cast<float>(renderer::get_screen_width());
    m_ratio_height = (quarter_pi<float>() *
        (static_cast<float>(renderer::get_screen_height()) /
        static_cast<float>(renderer::get_screen_width()))) /
        static_cast<float>(renderer::get_screen_height());
    //Free camera initilisation
    m_free_cam = free_camera();
    // Default the camera at the centre
    m_free_cam.set_position(vec3(10.0f));
    m_free_cam.set_target(vec3(0.0f));
    // Set field of view to ~80 degrees, aspect, near plane, far plane 
    m_free_cam.set_projection(pi<float>()*0.45f, m_aspect, 2.414f, 10000.0f);
    // Arc ball camera initilisation
    m_arc_cam = arc_ball_camera();
    m_arc_cam.set_position(vec3(0.0f));
    m_arc_cam.set_target(vec3(0.0f));
    m_arc_cam.set_distance(70.0f);
    m_arc_cam.set_projection(pi<float>()*0.45f, m_aspect, 2.414f, 10000.0f);
    m_arc_cam.rotate(-half_pi<float>()*0.25f, half_pi<float>()*0.5f);
    glfwSetScrollCallback(renderer::get_window(), ScrollWheel);
    // Create stuff required for post-processing
    m_bGaussian_blur = true;
    m_bMotion_blur = false;
    // Create framebuffers
    for (uint i = 0; i < 3; ++i)
    {
        m_frame_buffers[i] = frame_buffer(renderer::get_screen_width(),
                                          renderer::get_screen_height());
    }
    // Create a quad representing the screen
    std::vector<vec3> vertices
    {
        vec3( 1.0f,  1.0f, 0.0f),
        vec3(-1.0f,  1.0f, 0.0f),
        vec3(-1.0f, -1.0f, 0.0f),
        vec3( 1.0f, -1.0f, 0.0f),
    };
    std::vector<vec2> tex_coords
    {
        vec2(1.0f, 1.0f),
        vec2(0.0f, 1.0f),
        vec2(0.0f, 0.0f),
        vec2(1.0f, 0.0f)
    };
    m_screen_quad.set_type(GL_QUADS);
    m_screen_quad.add_buffer(vertices, BUFFER_INDEXES::POSITION_BUFFER);
    m_screen_quad.add_buffer(tex_coords, BUFFER_INDEXES::TEXTURE_COORDS_0);
    // Create post process shader programs
    m_display.add_shader("..\\Shaders\\simple_texture.vert", GL_VERTEX_SHADER);
    m_display.add_shader("..\\Shaders\\display.frag", GL_FRAGMENT_SHADER);
    m_display.build();
    m_blur.add_shader("..\\Shaders\\simple_texture.vert", GL_VERTEX_SHADER);
    m_blur.add_shader("..\\Shaders\\gaussian_blur.frag", GL_FRAGMENT_SHADER);
    m_blur.build();
    m_motion_blur.add_shader("..\\Shaders\\simple_texture.vert", GL_VERTEX_SHADER);
    m_motion_blur.add_shader("..\\Shaders\\motion_blur.frag", GL_FRAGMENT_SHADER);
    m_motion_blur.build();
}

CameraSystem::~CameraSystem()
{
    std::cerr << "NBODY: CameraSystem Destroyed." << std::endl;
}

void CameraSystem::ProcessInput(float dt)
{
    // Update active camera
    switch (m_active_cam)
    {
    case CameraSystem::FREE_CAM:
        UpdateFreeCam(dt);
        break;
    case CameraSystem::ARC_CAM:
        UpdateArcCam(dt);
        break;
    default:
        std::cerr << "NBODY: <file:CameraSys> Uknown cam_type!" << std::endl;
        _CrtDbgBreak();
        break;
    }
    // Toggle cursor lock
    if (KeyPressOnce(GLFW_KEY_LEFT_ALT)) 
    {
        if (m_lock_mouse = !m_lock_mouse)
        {
            glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        }
        else
        {
            glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        }
    }
    // Toggle active camera
    if (KeyPressOnce(GLFW_KEY_TAB))
    {
        m_active_cam = (m_active_cam == FREE_CAM) ? ARC_CAM : FREE_CAM;
    }
    // Toggle post process effects
    if (KeyPressOnce(GLFW_KEY_F1)) { m_bGaussian_blur = !m_bGaussian_blur; }
    if (KeyPressOnce(GLFW_KEY_F2)) { m_bMotion_blur   = !m_bMotion_blur; }
    // Reset mouse movement tracking
    m_delta_x = 0.0;
    m_delta_y = 0.0;
}

void CameraSystem::UpdateFreeCam(float dt)
{
    // Get new cursor position
    glfwGetCursorPos(renderer::get_window(), &m_current_x, &m_current_y);
    // If the mouse is active - track its position
    if (!m_lock_mouse)
    {
        // Get the amount the cursor has moved
        m_delta_x = (m_current_x - m_cursor_x) * m_ratio_width;
        m_delta_y = (m_current_y - m_cursor_y) * m_ratio_height;
        // Keep the old position
        m_cursor_x = m_current_x;
        m_cursor_y = m_current_y;
    }
    // Camera rotation input 
    if (KeyPressRepeat(GLFW_KEY_LEFT))  { m_delta_x -= 2.0f * dt; }
    if (KeyPressRepeat(GLFW_KEY_RIGHT)) { m_delta_x += 2.0f * dt; }
    if (KeyPressRepeat(GLFW_KEY_UP))    { m_delta_y -= 2.0f * dt; }
    if (KeyPressRepeat(GLFW_KEY_DOWN))  { m_delta_y += 2.0f * dt; }
    // Camera movement input
    vec3 translation(0.0f, 0.0f, 0.0f);
    if (KeyPressRepeat('W')) { translation.z += 10.0f * dt; }
    if (KeyPressRepeat('S')) { translation.z -= 10.0f * dt; }
    if (KeyPressRepeat('A')) { translation.x -= 10.0f * dt; }
    if (KeyPressRepeat('D')) { translation.x += 10.0f * dt; }
    if (KeyPressRepeat('Q')) { translation.y -= 10.0f * dt; }
    if (KeyPressRepeat('E')) { translation.y += 10.0f * dt; }
    if (KeyPressRepeat(GLFW_KEY_LEFT_SHIFT)) { translation *= 20.0f; }
    // Update camera using the input
    m_free_cam.rotate((float)(m_delta_x), -((float)(m_delta_y)));
    m_free_cam.move(translation);
    m_free_cam.update(dt);
}

void CameraSystem::UpdateArcCam(float dt)
{
    // Get new cursor position
    glfwGetCursorPos(renderer::get_window(), &m_current_x, &m_current_y);
    float translation{ 0.0f };
    // If the mouse is active - track its position
    if (!m_lock_mouse)
    {
        // Get the amount the cursor has moved
        m_delta_x = (m_current_x - m_cursor_x) * m_ratio_width;
        m_delta_y = (m_current_y - m_cursor_y) * m_ratio_height;
        // Keep the old position
        m_cursor_x = m_current_x;
        m_cursor_y = m_current_y;
        translation += static_cast<float>(scroll_wheel);
    }
    // Camera movement input
    if (KeyPressRepeat('W')) { m_delta_y -= 2.0f * dt; }
    if (KeyPressRepeat('S')) { m_delta_y += 2.0f * dt; }
    if (KeyPressRepeat('A')) { m_delta_x -= 2.0f * dt; }
    if (KeyPressRepeat('D')) { m_delta_x += 2.0f * dt; }
    if (KeyPressRepeat('Q')) { translation += 15.0f * dt; }
    if (KeyPressRepeat('E')) { translation -= 15.0f * dt; }
    if (KeyPressRepeat(GLFW_KEY_LEFT_SHIFT)) { translation *= 20.0f; }
    // Update camera using the input
    m_arc_cam.rotate(((float)(m_delta_y)), (float)(m_delta_x));
    m_arc_cam.move(translation);
    m_arc_cam.update(dt);
    // Reset scroll wheel input
    scroll_wheel = 0.0f;
}

const camera* CameraSystem::GetActiveCamera()
{
    switch (m_active_cam)
    {
    case CameraSystem::FREE_CAM:
        return &m_free_cam;
        break;
    case CameraSystem::ARC_CAM:
        return &m_arc_cam;
        break;
    default:
        std::cerr << "NBODY: <file:CameraSys> Uknown cam_type!" << std::endl;
        // Break in debug mode
#if DEBUG | _DEBUG
        __debugbreak();
#endif
        // Set free cam as default and return it
        m_active_cam = CAMERA_TYPES::FREE_CAM;
        return &m_free_cam;
        break;
    }
}

void CameraSystem::BeginPostProcess()
{
    //if (!m_background) { renderer::clear(); }
    // Flip frame and swap the frame buffers
    m_current_frame = !m_current_frame;
    renderer::set_render_target(m_frame_buffers[m_current_frame]);
    renderer::clear();
    if (m_bGaussian_blur) { glBlendFunc(GL_SRC_ALPHA, GL_ONE); }
}

void CameraSystem::GaussianBlur(const graphics_framework::camera *cam)
{
    // Set render target to a temporary buffer
    renderer::set_render_target(m_frame_buffers[2]);
    renderer::clear();
    // Use the gaussian blur shader program and send the current resolution
    renderer::bind(m_blur);
    mat4 MVP = mat4(1.0f);
    glUniformMatrix4fv(m_blur.get_uniform_location("MVP"), 1, GL_FALSE, value_ptr(MVP));
    glUniform2fv(m_blur.get_uniform_location("resolution"), 1,
        value_ptr(vec2(renderer::get_screen_width(), renderer::get_screen_height())));
    // Bind the frame buffer with objects that need to be blurred as a texture
    renderer::bind(m_frame_buffers[m_current_frame].get_frame(), 0);
    glUniform2fv(
        m_blur.get_uniform_location("blur_step"), 1, value_ptr(vec2(1.0f, 0.0f)));
    renderer::render(m_screen_quad);
    // Swap the render target
    renderer::set_render_target(m_frame_buffers[m_current_frame]);
    renderer::clear();
    // Bind the new horrizontally blurred frame to vertically blur
    renderer::bind(m_frame_buffers[2].get_frame(), 0);
    glUniform2fv(
        m_blur.get_uniform_location("blur_step"), 1, value_ptr(vec2(0.0f, 1.0f)));
    renderer::render(m_screen_quad);
}

void CameraSystem::MotionBlur(const graphics_framework::camera *cam)
{
    // Set render target back to screen and mix the frame buffers
    renderer::set_render_target();
    renderer::bind(m_motion_blur);
    mat4 MVP = mat4(1.0f);
    glUniformMatrix4fv(
        m_motion_blur.get_uniform_location("MVP"), 1, GL_FALSE, value_ptr(MVP));
    renderer::bind(m_frame_buffers[m_current_frame].get_frame(), 0);
    renderer::bind(m_frame_buffers[(!m_current_frame)].get_frame(), 1);
    glUniform1i(m_motion_blur.get_uniform_location("previous_frame"), 1);
    renderer::render(m_screen_quad);
}

void CameraSystem::EndPostProcess(const graphics_framework::camera *cam)
{
    // Apply n passes of gaussian blur
    if (m_bGaussian_blur)
    {
        for (glm::uint i = 0; i < 2; ++i)
        {
            GaussianBlur(cam);
        }
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }
    if (m_bMotion_blur)
    {
        MotionBlur(cam);
    }
    else
    {
        // Set render target back to screen and draw the frame buffer
        renderer::set_render_target();
        renderer::bind(m_display);
        mat4 MVP = mat4(1.0f);
        glUniformMatrix4fv(
            m_display.get_uniform_location("MVP"), 1, GL_FALSE, value_ptr(MVP));
        renderer::bind(m_frame_buffers[m_current_frame].get_frame(), 0);
        renderer::render(m_screen_quad);
    }
}