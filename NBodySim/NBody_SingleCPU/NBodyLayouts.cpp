/*
File: NBodyLayouts.cpp
--------------------------------------------------------------------------------
For details see NBodyLayouts.h
*/
#include "NBodyLayouts.h"

using namespace glm;

void Layouts::SpiralGalaxy(std::vector<NBody> &nbodies, uint size)
{
    nbodies.resize(size);
    // Initialise defaults
    vec3 pos{ 0.0f, 0.0f, 0.0f };
    vec3 vel{ 0.0f, 0.0f, 0.0f };
    float mass{ 100000000000.0f };
    mat4 spin = rotate(mat4(1.0f), half_pi<float>(), vec3(0.0f, 1.0f, 0.0f));
    // Form a spiral galaxy of NBodies
    for (uint i = 0; i < nbodies.size(); ++i)
    {
        float p = static_cast<float>(i+10);
        float scale = 2.0f - p / nbodies.size()*10.0f;
        pos = vec3(sinf(p)*p*0.15f, sinf(p*p)*p*0.01f+0.01f, cosf(p)*p*0.15f);
        // Add spin to the galaxy
        vel = normalize(vec3(vec4(-pos, 1.0f)*spin))*scale;
        nbodies[i] = NBody(pos, vel, mass);
    }
}

void Layouts::TwinSpiralGalaxies(std::vector<NBody> &nbodies, uint size)
{
    nbodies.resize(size);
    // Initialise defaults
    vec3 pos{ 0.0f, 0.0f, 0.0f };
    vec3 vel{ 0.0f, 0.0f, 0.0f };
    vec3 offset = vec3(static_cast<float>(std::cbrt(size*size))*0.5f);
    float mass{ 100000000000.0f };
    mat4 spin = rotate(mat4(1.0f), quarter_pi<float>(), vec3(0.0f, 1.0f, 0.0f));
    uint n{ 0 };
    // Form 1st spiral galaxy of NBodies with an positive offset
    for (uint i = 0; i < nbodies.size()/2; ++i, ++n)
    {
        float p = static_cast<float>(i + 16);
        float scale = 2.0f - p / nbodies.size()*16.0f;
        pos = vec3(sinf(p)*p*0.15f, sinf(p*p)*p*0.04f+0.01f, cosf(p)*p*0.15f);
        pos += offset;
        // Add spin to the galaxy
        vel = -normalize(vec3(vec4(offset-pos, 1.0f)*spin))*scale;
        vel.y -= 5.0f;
        nbodies[n] = NBody(pos, vel, mass);
    }
    // Form 2nd spiral galaxy of NBodies with an negative offset
    for (uint i = 0; i < nbodies.size()/2; ++i, ++n)
    {
        float p = static_cast<float>(i + 16);
        float scale = 2.0f - p / nbodies.size()*16.0f;
        pos = vec3(sinf(p)*p*0.15f, sinf(p*p)*p*0.04f+0.01f, cosf(p)*p*0.15f);
        pos -= offset;
        // Add spin to the galaxy
        vel = normalize(vec3(vec4(offset+pos, 1.0f)*spin))*scale;
        vel.y += 5.0f;
        nbodies[n] = NBody(pos, vel, mass);
    }
}

void Layouts::CubeDistribution(std::vector<NBody> &nbodies, uint size)
{
    nbodies.resize(size);
    uint croot_size{ static_cast<uint>(std::cbrt(size)) };
    // Initiliase default values
    vec3  pos{ 0.0f, 0.0f, 0.0f };
    float mass{ 100000000000.0f };
    float offset{ 2.0f };
    // Make a cuboid from points
    uint n{ 0 };
    for (uint i = 0; n < size; ++i)
    for (uint j = 0; j < croot_size && n < size; ++j)
    for (uint k = 0; k < croot_size && n < size; ++k, ++n)
    {
        pos = vec3(j*offset, i*offset, k*offset) - vec3(croot_size - offset*0.5f);
        nbodies[n] = NBody(pos, mass);
    }
}

void Layouts::RandomDistribution(std::vector<NBody> &nbodies, uint size)
{
    nbodies.resize(size);
    // Default values
    vec3 position{ 0.0f, 0.0f, 0.0f };
    float mass{ 200000000000.0f };
    float offset{ static_cast<float>(size)*0.2f };
    // Create random number generator
    std::default_random_engine gen;
    std::uniform_real<float> dist(-offset, offset);
    std::uniform_real<float> dist_mass(mass, mass*2.0f);
    // Distribute NBodies randomly
    for (uint i = 0; i < size; ++i)
    {
        position = vec3(dist(gen), dist(gen), dist(gen));
        while (glm::length2(position) > offset*offset)
        {
            position = vec3(dist(gen), dist(gen), dist(gen));
        }
        nbodies[i] = NBody(position, dist_mass(gen));
    }
}

void Layouts::Singularity(std::vector<NBody> &nbodies, uint size)
{
    nbodies.resize(size);
    // Default values
    vec3 position{ 0.0f, 0.0f, 0.0f };
    float mass{ 200000000000.0f };
    float offset{ static_cast<float>(size)*0.016f };
    // Create random number generator
    std::default_random_engine gen;
    std::uniform_real<float> dist_pos(-offset, offset);
    std::uniform_real<float> dist_mass(mass, mass*2.0f);
    // Distribute NBodies randomly within a tight sphere
    for (uint i = 0; i < size; ++i)
    {
        position = vec3(dist_pos(gen), dist_pos(gen), dist_pos(gen));
        while (glm::length2(position) > offset*offset)
        {
            position = vec3(dist_pos(gen), dist_pos(gen), dist_pos(gen));
        }
        nbodies[i] = NBody(position, dist_mass(gen));
    }
}

// Layouts 8 NBodies diagonally apart
void Layouts::TestLayout(std::vector<NBody> &nbodies, uint size)
{
    nbodies.resize(size);
    // Setup corner positions
    vec3 positions[4]
    {
        vec3(10.0f, 10.0f, 10.0f),
        vec3(10.0f, 10.0f, -10.0f),
        vec3(-10.0f, 10.0f, -10.0f),
        vec3(-10.0f, 10.0f, 10.0f),
    };
    vec3 velocity{ 0.0f, 0.0f, 0.0f };
    float mass{ 100000000000.0f };
    // Place two bodies at a time: top and bottom
    for (uint i = 0; i < 4; i++)
    {
        nbodies[i]   = NBody( positions[i], mass);
        nbodies[4+i] = NBody(-positions[i], mass);
    }
}