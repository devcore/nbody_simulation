/*--------------------------------------------------------\
| File: InputHandler.h                                    |
|x-------------------------------------------------------x|
| Details: Handles key press events from GLFW by storing, |
| a table of key states and checking their changes.       |
|x-------------------------------------------------------x|
| Author:                              Andrius Jefremovas |
\--------------------------------------------------------*/
#ifndef INPUT_HANDLER_H
#define INPUT_HANDLER_H

#include <graphics_framework.h>

// Don't know why GLFW does not offer access to a key press once or their table.
static bool key_states[GLFW_KEY_LAST+1];
static float scroll_wheel = 0.0f;
/* 
    Returns true every frame a key is held down. 
    -Usage: if(KeyPressRepeat('1')) foo();
*/
static bool KeyPressRepeat(int key)
{
    if (glfwGetKey(graphics_framework::renderer::get_window(), key))
    {
        return true;
    }
    return false;
}
/* 
    Returns true when a key is first pressed.
    -Usage: if(KeyPressOnce('1')) foo();
*/
static bool KeyPressOnce(int key)
{
    if (glfwGetKey(graphics_framework::renderer::get_window(), key))
    {
        return (key_states[key] ? false : (key_states[key] = true));
    }
    return (key_states[key] = false);
}
/* 
    Executes the callback fuction every frame a key is held down.
    -Usage: KeyPressOnceCBKeyPressRepeatCB('1', [](){ foo(); });
*/
static void KeyPressRepeatCB(int key, void(*pFunc)())
{
    if (glfwGetKey(graphics_framework::renderer::get_window(), key))
    {
        pFunc();
    }
}
/*
    Executes the callback when a key is first pressed.
    -Usage: KeyPressOnceCB('1', [](){ foo(); });
*/
static void KeyPressOnceCB(int key, void(*pFunc)())
{
    if (glfwGetKey(graphics_framework::renderer::get_window(), key))
    {
        if (key_states[key] ? false : (key_states[key] = true))
        {
            pFunc();
        }
    }
    else
    {
        key_states[key] = false;
    }
}

static void ScrollWheel(GLFWwindow* window, double up, double down)
{
    scroll_wheel = static_cast<float>(down)+0.01f;
}

#endif /* !INPUT_HANDLER_H */