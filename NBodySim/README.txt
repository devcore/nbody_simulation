x-Goal--------------------------------------------------x
 Increase the performance of this application using 
 hardware acceleration, then measure and compare it.
x-Description-------------------------------------------x
 Solution contains 5 implementations of the nbody problem:
 -> CONTROL VERSION (Single_CPU): is the base version using
 which performance gains are calculated.
 
 -> SSE VERSION (Single_CPU_SIMD): using 128-bit SIMD 
 registers to perform 4 wide operations. Two variants:
 AoS (array of structs) and SoA (struct of arrays).
 
 -> Parallel VERSION (Multi_CPU): uses multiple threads to
 process nbodies in parallel. Three variants: C++11 threads,
 OpenMP parallel for loop and PPL (Parallel Pattern Library).
 
 -> Parallel SIMD VERSION (Multi_CPU_SIMD): combines the
 SoA SIMD approach with PPL's parallel for loop.
 
 >NOTE<: Optimization is turned off only for the integration     
 function block to avoid skewed results from auto thread  
 and SIMD optimization. 
x-------------------------------------------------------x
 CONTROLS:                                               
 SPACEBAR - Simulation Start/Pause                       
 TAB      - Changes camera type                          
 Alt      - Unlock/Locks mouse                           
 ESC      - Closes application                           
 F1/F2    - Disable/Enable blur/motion blur              
 1-5      - Change NBody layout                          
 6        - Adds NBodies while FPS is above 60           
x-------------------------------------------------------x
 ARC-BALL Camera:                                        
 WASD/Mouse - Rotation                                   
 QE/Scroll  - Zoom/Distance                              
 Shift      - HOLD to increase speed of zooming          
x-------------------------------------------------------x
 FREE Camera                                             
 WASD QE      - Movement                                 
 Mouse/Arrows - Rotation                                  
 Shift        - HOLD to increase movement speed            
x-------------------------------------------------------x     
 Author:                              Andrius Jefremovas
x-------------------------------------------------------x
    _____    
    \   /    
    /   \    
   /  A  \   
  /  / \  \  
 /  /   \  \ 
/  /_____\  \
\___________/
