/*
File: NBodyLayouts.cpp
--------------------------------------------------------------------------------
For details see NBodyLayouts.h
*/
#include "NBodyLayouts.h"

using namespace glm;

void Layouts::SpiralGalaxy(std::vector<NBody> &nbodies, const uint size)
{
    nbodies.resize(size);
    // Spin of the galaxy
    mat4 spin = rotate(mat4(1.0f), half_pi<float>(), vec3(0.0f, 1.0f, 0.0f));
    // Form a spiral galaxy of NBodies
    for (uint i = 0; i < size; ++i)
    {
        float p = static_cast<float>(i + 10);
        float scale = 2.0f - p / size*16.0f;
        nbodies[i].pos_mass = vec4(sinf(p)*p*0.15f,         
                                   sinf(p*p)*p*0.01f+0.01f, 
                                   cosf(p)*p*0.15f,         
                                   1.0f); // Temp to avoid normalization issue
        nbodies[i].vel = normalize(-nbodies[i].pos_mass*spin)*scale;
        nbodies[i].pos_mass.w = 1000000000000.0f;
    }
}

void Layouts::TwinSpiralGalaxies(std::vector<NBody> &nbodies, const uint size)
{
    nbodies.resize(size);
    // Distance between the galaxies
    vec4 offset = vec4(static_cast<float>(std::cbrt(size*size)));
    offset.w = 0.0f;
    // Spin of the galaxy
    mat4 spin = rotate(mat4(1.0f), quarter_pi<float>(), vec3(0.0f, 1.0f, 0.0f));
    uint n{ 0 };
    // Form 1st spiral galaxy of NBodies with an positive offset
    for (uint i = 0; i < size / 2; ++i, ++n)
    {
        float p = static_cast<float>(i + 16);
        float scale = 2.0f - p / size*32.0f;
        nbodies[n].pos_mass = vec4(sinf(p)*p*0.15f, 
                                   sinf(p*p)*p*0.04f+0.01f, 
                                   cosf(p)*p*0.15f,
                                   1.0f);
        nbodies[n].pos_mass += offset;
        nbodies[n].vel = -normalize(offset-nbodies[n].pos_mass*spin)*scale;
        nbodies[n].vel.y -= 5.0f;
        nbodies[n].pos_mass.w = 5000000000000.0f;
    }
    // Form 2nd spiral galaxy of NBodies with an negative offset
    for (uint i = 0; i < size / 2; ++i, ++n)
    {
        float p = static_cast<float>(i + 16);
        float scale = 2.0f - p / size*32.0f;
        nbodies[n].pos_mass = vec4(sinf(p)*p*0.15f, 
                                   sinf(p*p)*p*0.04f+0.01f, 
                                   cosf(p)*p*0.15f,
                                   1.0f);
        nbodies[n].pos_mass -= offset;
        // Add spin to the galaxy
        nbodies[n].vel = normalize(offset+nbodies[n].pos_mass*spin)*scale;
        nbodies[n].vel.y += 5.0f;
        nbodies[n].pos_mass.w = 5000000000000.0f;
    }
}

void Layouts::CubeDistribution(std::vector<NBody> &nbodies, const uint size)
{
    nbodies.resize(size);
    uint croot_size{ static_cast<uint>(std::cbrt(size)) };
    // Offset between nbodies
    float offset{ 2.0f };
    // Make a cuboid from points
    uint n{ 0 };
    for (uint i = 0; n < size; ++i)
    for (uint j = 0; j < croot_size && n < size; ++j)
    for (uint k = 0; k < croot_size && n < size; ++k, ++n)
    {
        vec3 new_pos = vec3(j*offset, i*offset, k*offset) - 
                       vec3(croot_size-offset*0.5f);
        nbodies[n].pos_mass = vec4(new_pos, 100000000000.0f);
        nbodies[n].vel = vec4(0.0f);
    }
}

void Layouts::RandomDistribution(std::vector<NBody> &nbodies, const uint size)
{
    nbodies.resize(size);
    // Minimal mass
    float mass{ 1000000000000.0f };
    // Radius of the distribution
    float offset{ static_cast<float>(size)*0.02f };
    // Create random number generator
    std::default_random_engine gen;
    std::uniform_real<float> dist(-offset, offset);
    std::uniform_real<float> dist_mass(mass, mass*2.0f);
    // Distribute NBodies randomly
    for (uint i = 0; i < size; ++i)
    {
        vec3 position = vec3(dist(gen), dist(gen), dist(gen));
        while (glm::length2(position) > offset*offset)
        {
            position = vec3(dist(gen), dist(gen), dist(gen));
        }
        nbodies[i].pos_mass = vec4(position, dist_mass(gen));
        nbodies[i].vel = vec4(0.0f);
    }
}

void Layouts::Singularity(std::vector<NBody> &nbodies, const uint size)
{
    nbodies.resize(size);
    // Minimal mass
    float mass{ 500000000000.0f };
    // Radius of distribution
    float offset{ static_cast<float>(size) * 0.001f };
    // Create random number generator
    std::default_random_engine gen;
    std::uniform_real<float> dist_pos(-offset, offset);
    std::uniform_real<float> dist_mass(mass, mass*4.0f);
    // Distribute NBodies randomly within a tight sphere
    for (uint i = 0; i < size; ++i)
    {
        vec3 position = vec3(dist_pos(gen), dist_pos(gen), dist_pos(gen));
        while (glm::length2(position) > offset*offset)
        {
            position = vec3(dist_pos(gen), dist_pos(gen), dist_pos(gen));
        }
        nbodies[i].pos_mass = vec4(position, dist_mass(gen));
        nbodies[i].vel = vec4(0.0f);
    }
}

// Layouts 8 NBodies diagonally apart
void Layouts::TestLayout(std::vector<NBody> &nbodies, uint size)
{
    nbodies.resize(size);
    // Setup corner positions
    vec3 positions[4]
    {
        vec3( 10.0f, 10.0f,  10.0f),
        vec3( 10.0f, 10.0f, -10.0f),
        vec3(-10.0f, 10.0f, -10.0f),
        vec3(-10.0f, 10.0f,  10.0f),
    };
    float mass{ 100000000000.0f };
    // Place two bodies at a time: top and bottom
    for (uint i = 0; i < 4; i++)
    {
        nbodies[i].pos_mass   = vec4( positions[i], mass);
        nbodies[4+i].pos_mass = vec4(-positions[i], mass);
    }
}