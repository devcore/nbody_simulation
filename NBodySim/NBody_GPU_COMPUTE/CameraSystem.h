/*--------------------------------------------------------\
| File: CameraSystem.h                                    |
|x-------------------------------------------------------x|
| Details: Manages switching between cameras and their    |
| movement based on user input. Also performs the post-   |
| process effects: Gaussian blur and motion blur.         |
|x-------------------------------------------------------x|
| Author:                              Andrius Jefremovas |
| Description:        Camera movement and post-processing.|
\--------------------------------------------------------*/
#ifndef CAMERA_SYSTEM_H
#define CAMERA_SYSTEM_H

#include <graphics_framework.h>

struct CameraSystem
{
    CameraSystem();
    ~CameraSystem();
    // Controls the active camera based on users input.
    void ProcessInput(float dt);
    // Changes the render target and swaps frame buffers.
    void BeginPostProcess();
    // Performs post-process effect on current frame buffer
    void EndPostProcess(const graphics_framework::camera *cam);
    // Returns camera which is currently in use.
    const graphics_framework::camera* GetActiveCamera();
private:
    // List of available camera types.
    enum CAMERA_TYPES
    {
        FREE_CAM = 0,
        ARC_CAM = 1,
    };
    /* User Control */
    CAMERA_TYPES m_active_cam;
    graphics_framework::free_camera m_free_cam;
    graphics_framework::arc_ball_camera m_arc_cam;
    bool m_lock_mouse;  // Mouse capture enable/disable
    float m_aspect;     // Screen aspect ratio
    // Free-cam parameters for mouse movement to camera rotation
    double m_cursor_x, m_cursor_y;         // X-Y cursor position             
    double m_current_x, m_current_y;       // X-Y current cursor position
    double m_delta_x, m_delta_y;           // X-Y amount the cursor moved
    double m_ratio_width, m_ratio_height;  // Screen width-height ratio
    /* Post Process */
    bool m_bGaussian_blur;
    bool m_bMotion_blur;
    int m_blur_num;
    graphics_framework::effect m_display;       // Displays frame buffer
    graphics_framework::effect m_blur;          // Gaussian blur shader
    graphics_framework::effect m_motion_blur;   // Motion blur shader
    graphics_framework::geometry m_screen_quad; // Display quad
    graphics_framework::frame_buffer m_frame_buffers[3];
    glm::uint m_current_frame;

    void UpdateFreeCam(float dt);
    void UpdateArcCam(float dt);

    void GaussianBlur(const graphics_framework::camera *cam);
    void MotionBlur(const graphics_framework::camera *cam);
};

#endif /* !CAMERA_SYSTEM_H */