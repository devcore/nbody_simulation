/*--------------------------------------------------------\
| File: NBodySystem.h                                     |
|x-------------------------------------------------------x|
| Details: Creates a number of NBodies that attract each  |
| other with O(n^2) computational complexity. Attraction  |
| is calculated using: a = G*M_j*R_ij / (|R_ij|^2+E)^3/2  |
| Where: a - acceleration, G - gravitational constant,    |
| M - mass, R - distance and E - smoothing factor.        |
| Formula from: NVidia GPU gems 3 NBody Simulation        |
|x-------------------------------------------------------x|
| TILED 0:                                                |
| Stores the whole nbody buffer in GPU memory.            |
| TILED 1:                                                |
| Store two separate buffers: positions and velocities in |
| addition it also uses shared memory.                    |
| VERSION CHANGE:                                         |
| Change either #defined value below to 1 and the other 0 |
|x-------------------------------------------------------x|
| Author:                              Andrius Jefremovas |
| Description:    Simulates NBody attraction between them.|
\--------------------------------------------------------*/
#ifndef NBODY_SYSTEM_H
#define NBODY_SYSTEM_H

#include <graphics_framework.h>

#define TILED 1

// The defination of a single NBody
__declspec(align(16)) struct NBody
{
    glm::vec4 pos_mass; // Position & Mass | 16 |
    glm::vec4 vel;      // Velocity        | 32 |
    // Default constructor
    NBody() : pos_mass(0.0f), vel(0.0f) { }
};

struct NBodySystem
{
    NBodySystem();
    ~NBodySystem();
    // Resets nbodies to new locations and resizes OpenGL buffer
    void ResetToNewLayout(const glm::uint size,
         void(*pFunc)(std::vector<NBody> &nbodies, const glm::uint size));
    // Takes input to control simulation and calls integrate/streambuffer
    void Update(const float delta_time);
    // Renders each NBody using the buffer of positions to create quads
    void Render(const graphics_framework::camera *cam);
private:
    // Performs NBody attraction and new position calculations
    void Integrate(const float delta_time);
    // Test how many NBodies can be simulated withing 60 fps
    void NBodyLimitTest(const float delta_time);
    // Test iteration timings of various numbers of nbodies
    void PerformanceTest();
    // Performs integration for N iterations and print the results to a file
    void TestIntegration(const glm::uint iter);
    /* System properties */
    bool m_running;
    bool m_limit_test;
    GLuint m_vao;
#if TILED
    GLuint m_vbo_pos;
    GLuint m_vbo_vel;
#else
    GLuint m_vbo;
#endif
    glm::uint m_num_nbodies;
    // NBody buffer
    std::vector<NBody> m_nbodyBuffer;
    // NBody display properties
    graphics_framework::effect m_billboard;
    graphics_framework::effect m_nbody_compute;
    //graphics_framework::geometry m_quad;
};

#endif /* !NBODY_SYSTEM_H */