/*
File: NBodySystem.cpp
--------------------------------------------------------------------------------
For details see NBodySystem.h
*/
#include "NBodySystem.h"
#include "NBodyLayouts.h"
#include "InputHandler.h"
#include <thread>

using namespace glm;
using namespace graphics_framework;

// Constants
const float g_gravitation{ 6.674f * powf(10.0f, -11) };
const float g_smoothing{ 0.8f };
const float g_damping{ 0.99987f };
const uint work_chunk{ 256 };
// Quick macro for buffer size in bytes
#if TILED
#define BUFFER_SIZE m_nbodyBuffer.size()*sizeof(vec4)
#else
#define BUFFER_SIZE m_nbodyBuffer.size()*sizeof(NBody)
#endif

NBodySystem::NBodySystem()
{
    std::cerr << "NBODY: NBodySystem created." << std::endl;
    // Initialise default settings
    m_running = false;
    m_limit_test = false;
    m_num_nbodies = 256;
    m_nbodyBuffer.reserve(65536);
    // Load default cube distribution
    Layouts::SpiralGalaxy(m_nbodyBuffer, m_num_nbodies);
    // Create shader programs
    m_billboard.add_shader("..\\Shaders\\billboard.vert", GL_VERTEX_SHADER);
    m_billboard.add_shader("..\\Shaders\\billboard.geom", GL_GEOMETRY_SHADER);
    m_billboard.add_shader("..\\Shaders\\billboard.frag", GL_FRAGMENT_SHADER);
    m_billboard.build();
#if TILED
    m_nbody_compute.add_shader("..\\Shaders\\nbody_compute_tiled.comp", GL_COMPUTE_SHADER);
#else
    m_nbody_compute.add_shader("..\\Shaders\\nbody_compute.comp", GL_COMPUTE_SHADER);
#endif
    m_nbody_compute.build();

    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);
#if TILED
    glGenBuffers(1, &m_vbo_pos);
    glGenBuffers(1, &m_vbo_vel);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_vbo_vel);
    std::vector<vec4> temp(m_num_nbodies);
    for (uint i = 0; i < m_num_nbodies; i++)
    {
        temp[i] = m_nbodyBuffer[i].vel;
    }
    glBufferData(GL_SHADER_STORAGE_BUFFER, BUFFER_SIZE, &temp[0], GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, m_vbo_pos);
    for (uint i = 0; i < m_num_nbodies; i++)
    {
        temp[i] = m_nbodyBuffer[i].pos_mass;
    }
    glBufferData(GL_ARRAY_BUFFER, BUFFER_SIZE, &temp[0], GL_DYNAMIC_DRAW);

    const GLuint ssbos[2] = { m_vbo_pos, m_vbo_vel };
    glBindBuffersBase(GL_SHADER_STORAGE_BUFFER, 0, 2, ssbos);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(vec4), 0);
#else
    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);

    glGenBuffers(1, &m_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBufferData(GL_ARRAY_BUFFER, BUFFER_SIZE, &m_nbodyBuffer[0], GL_DYNAMIC_DRAW);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, m_vbo);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(NBody), 0);
#endif

#if 0  // Performance test
    PerformanceTest();
#elif 0  // Unit test
    TestIntegration(1000);
#endif
    std::cerr << "NBODY: NBody count: " << m_nbodyBuffer.size() << std::endl;
}

NBodySystem::~NBodySystem()
{
    std::cerr << "NBODY: NBodySystem Destroyed." << std::endl;
    m_nbodyBuffer.clear();
    glDeleteVertexArrays(1, &m_vao);
#if TILED
    glDeleteBuffers(1, &m_vbo_pos);
    glDeleteBuffers(1, &m_vbo_vel);
#else
    glDeleteBuffers(1, &m_vbo);
#endif
}

void NBodySystem::ResetToNewLayout(const uint size,
     void(*pfunc)(std::vector<NBody> &nbodies, const uint size))
{
    m_running = false; // Pause the simulation
#if TILED
    // Mark current VBO data as invalid
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo_vel);
    glBufferData(GL_ARRAY_BUFFER, BUFFER_SIZE, nullptr, GL_DYNAMIC_DRAW);
    m_nbodyBuffer.clear();
    pfunc(m_nbodyBuffer, size); // Layout NBodies in new pattern
    // Reallocate new buffer with the new size and update it with new positions
    glBufferData(GL_ARRAY_BUFFER, BUFFER_SIZE, nullptr, GL_DYNAMIC_DRAW);
    vec4* ptr_mapbuffer2 = (vec4*)glMapBufferRange(GL_ARRAY_BUFFER, 0, BUFFER_SIZE,
        GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT);
    for (uint i = 0; i < m_num_nbodies; i++)
    {
        ptr_mapbuffer2[i] = m_nbodyBuffer[i].vel;
    }
    glUnmapBuffer(GL_ARRAY_BUFFER);
    // Mark current VBO data as invalid
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo_pos);
    glBufferData(GL_ARRAY_BUFFER, BUFFER_SIZE, nullptr, GL_DYNAMIC_DRAW);
    m_nbodyBuffer.clear();               
    pfunc(m_nbodyBuffer, size); // Layout NBodies in new pattern
    // Reallocate new buffer with the new size and update it with new positions
    glBufferData(GL_ARRAY_BUFFER, BUFFER_SIZE, nullptr, GL_DYNAMIC_DRAW);
    vec4* ptr_mapbuffer = (vec4*)glMapBufferRange(GL_ARRAY_BUFFER, 0, BUFFER_SIZE,
                                    GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT);
    for (uint i = 0; i < m_num_nbodies; i++)
    {
        ptr_mapbuffer[i] = m_nbodyBuffer[i].pos_mass;
    }
    glUnmapBuffer(GL_ARRAY_BUFFER);
#else
    // Mark current VBO data as invalid
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBufferData(GL_ARRAY_BUFFER, BUFFER_SIZE, nullptr, GL_DYNAMIC_DRAW);
    m_nbodyBuffer.clear();               
    pfunc(m_nbodyBuffer, size); // Layout NBodies in new pattern
    // Reallocate new buffer with the new size and update it with new positions
    glBufferData(GL_ARRAY_BUFFER, BUFFER_SIZE, nullptr, GL_DYNAMIC_DRAW);
    void* ptr_mapbuffer = glMapBufferRange(GL_ARRAY_BUFFER, 0, BUFFER_SIZE,
                                  GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT);
    memcpy(ptr_mapbuffer, &m_nbodyBuffer[0], BUFFER_SIZE);
    glUnmapBuffer(GL_ARRAY_BUFFER);
#endif
    std::cerr << "NBODY: NBody count: " << m_nbodyBuffer.size() << std::endl;
}

void NBodySystem::Update(const float delta_time)
{
    // Change NBody layouts
    if (KeyPressOnce('1')) { ResetToNewLayout(m_num_nbodies, Layouts::SpiralGalaxy); }
    if (KeyPressOnce('2')) { ResetToNewLayout(m_num_nbodies, Layouts::TwinSpiralGalaxies); }
    if (KeyPressOnce('3')) { ResetToNewLayout(m_num_nbodies, Layouts::CubeDistribution); }
    if (KeyPressOnce('4')) { ResetToNewLayout(m_num_nbodies, Layouts::RandomDistribution); }
    if (KeyPressOnce('5')) { ResetToNewLayout(m_num_nbodies, Layouts::Singularity); }
    // Max NBody routine
    if (KeyPressOnce('6')) { m_limit_test = true; }
    if (KeyPressOnce('T')) { TestIntegration(100); }
    // Pause/Unpause simulation
    if (KeyPressOnce(GLFW_KEY_SPACE)) { m_running = !m_running; }
    if (m_running)
    { 
        Integrate(delta_time);
    }
    if (m_limit_test)
    {
        m_running = true;
        NBodyLimitTest(delta_time);
    }
}

#pragma optimize("", off)
void NBodySystem::Integrate(const float delta_time)
{
    renderer::bind(m_nbody_compute);
    glUniform1f(m_nbody_compute.get_uniform_location("delta_time"), delta_time);
    glDispatchCompute(m_num_nbodies/work_chunk, 1, 1);
    glMemoryBarrier(GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT);
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
    // Fetch and check results
    //std::vector<vec4> data(m_num_nbodies);
    //glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_vbo_vel);
    //glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, BUFFER_SIZE, &data[0]);
    //__debugbreak();
}
#pragma optimize("", on)

void NBodySystem::Render(const camera *cam)
{
    // Render a quad billboard for each nbody point
    renderer::bind(m_billboard);
    const mat4 V = cam->get_view();
    const mat4 P = cam->get_projection();
    const vec3 cam_pos = cam->get_position();
    glUniformMatrix4fv(
        m_billboard.get_uniform_location("V"), 1, GL_FALSE, value_ptr(V));
    glUniformMatrix4fv(
        m_billboard.get_uniform_location("P"), 1, GL_FALSE, value_ptr(P));
    glUniform3fv(
        m_billboard.get_uniform_location("camera_pos"), 1, value_ptr(cam_pos));
    glBindVertexArray(m_vao);
    glDrawArrays(GL_POINTS, 0, m_num_nbodies);
}

void NBodySystem::NBodyLimitTest(const float delta_time)
{
    const int iter_count = 10;
    const float target_FPS = 0.015f;
    static int hit_count = iter_count;

    if (hit_count > 0)
    {
        hit_count -= 1;
    }
    else
    {
        if (delta_time < target_FPS)
        {
            m_num_nbodies += 256;
            ResetToNewLayout(m_num_nbodies, Layouts::CubeDistribution);
            std::cerr << "NBODY: New NBody count: " << m_nbodyBuffer.size() <<
                "\n" << "Time: " << delta_time << " target: " << target_FPS << std::endl;
        }
        else
        {
            m_limit_test = false;
            m_running = false;
            std::cerr << "NBODY: New NBody count: " << m_nbodyBuffer.size() <<
                "\n" << "Time: " << delta_time << " target: " << target_FPS << std::endl;
        }
        hit_count = iter_count;
    }
}

void NBodySystem::PerformanceTest()
{
    using namespace std::chrono;
    // Pre-create variables used for timing and setup the system
    GLuint time_query;
    GLuint64 time_elapsed;
    glGenQueries(1, &time_query);
    double total{ 0.0 };
    const uint iter_count = 10000;
    const uint nbody_count[4]{ 1048, 2048, 4096, 8192 };
    // Create a file to store the data
    std::ofstream data_file("NBody_performance_int_tiled_128.csv", std::ofstream::out);
    data_file << "NBodies, 1024, 2048, 4096, 8192\n";
    data_file << "Test_Name, ";
    // Test with 4 different sizes for 1000 iterations
    for (uint n = 0; n < 4; ++n)
    {
        m_num_nbodies = nbody_count[n];
        ResetToNewLayout(m_num_nbodies, Layouts::SpiralGalaxy);;
        glBeginQuery(GL_TIME_ELAPSED, time_query);
        for (uint i = 0; i < iter_count; ++i)
        {
            Integrate(0.2f);
        }
        glEndQuery(GL_TIME_ELAPSED);
        glGetQueryObjectui64v(time_query, GL_QUERY_RESULT, &time_elapsed);
        total = static_cast<double>(time_elapsed);
        std::cerr << n << " " << total << std::endl;
        total /= iter_count; // Compute average
        total *= 0.000001; // Convert to milliseconds
        data_file << total << ", ";
        std::cerr << n << ": " << total << std::endl;
    }
    data_file.close();
}

void NBodySystem::TestIntegration(uint iter)
{
    using namespace std;
    // Space out 8 nbodies evenly
    m_num_nbodies = 8;
    ResetToNewLayout(m_num_nbodies, Layouts::TestLayout);
    // Integrate for N times
    for (uint i = 0; i < iter; ++i)
    {
        renderer::bind(m_nbody_compute);
        glUniform1f(m_nbody_compute.get_uniform_location("delta_time"), 0.01f);
        glDispatchCompute(m_num_nbodies, 1, 1);
        glMemoryBarrier(GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT);
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
    }
#if TILED
    std::vector<vec4> position_buffer(m_num_nbodies);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_vbo_pos);
    glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, BUFFER_SIZE, &position_buffer[0]);
    // Store the new positions in a file
    string file_name = "GPU_TILED_IntegrationTest_"+to_string(iter)+string(".csv");
    ofstream data_file(file_name, ofstream::out);
    data_file << "Positions after: " << iter << "\n";
    data_file << "x,y,z" << "\n";
    for (uint i = 0; i < m_num_nbodies; i++)
    {
        data_file << position_buffer[i].x << ",";
        data_file << position_buffer[i].y << ",";
        data_file << position_buffer[i].z << ",";
        data_file << "\n";
    }
#else
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_vbo);
    glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, BUFFER_SIZE, &m_nbodyBuffer[0]);
    // Store the new positions in a file
    string file_name = "GPU_IntegrationTest_" + to_string(iter) + string(".csv");
    ofstream data_file(file_name, ofstream::out);
    data_file << "Positions after: " << iter << "\n";
    data_file << "x,y,z" << "\n";
    for (uint i = 0; i < m_num_nbodies; i++)
    {
        data_file << m_nbodyBuffer[i].pos_mass.x << ",";
        data_file << m_nbodyBuffer[i].pos_mass.y << ",";
        data_file << m_nbodyBuffer[i].pos_mass.z << ",";
        data_file << "\n";
    }
#endif
    data_file.close();
}