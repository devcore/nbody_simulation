/*--------------------------------------------------------\
| File: main.cpp                                          |
|x-------------------------------------------------------x|
| Details: Implementation of an NBody simulation. This    |
| versions uses Streaming SIMD Extensions(SSE) to operate |
| four sets of data with a single instruction; 2 variants |
| were implemented: the first uses 3 lanes of a register  |
| to compute and discards the last value. The second      |
| implementation adapts the data structions for ideal SSE |
| support packing 4 NBodies at a time.                    |
| To use the first version go to nbodysystem.h            |
|x-------------------------------------------------------x|
| CONTROLS:                                               |
| SPACEBAR - Simulation Start/Pause                       |
| TAB      - Changes camera type                          |
| Alt      - Unlock/Locks mouse                           |
| ESC      - Closes application                           |
| F1/F2    - Disable/Enable blur/motion blur              |
| 1-5      - Change NBody layout                          |
| 6        - Adds NBodies while FPS is above 60           |
|x-------------------------------------------------------x|
| ARC-BALL Camera:                                        |
| WASD/Mouse - Rotation                                   |
| QE/Scroll  - Zoom/Distance                              |
| Shift      - HOLD to increase speed of zooming          |
|x-------------------------------------------------------x|      _____
| FREE Camera                                             |      \   /  
| WASD QE      - Movement                                 |      /   \  
| Mouse/Arrows - Rotation                                 |     /  A  \  
| Shift        - HOLD to increase movement speed          |    /  / \  \  
|x-------------------------------------------------------x|   /  /   \  \   
| Author:                              Andrius Jefremovas |  /  /_____\  \  
| Description:    Entry point, creates systems and links. |  \___________/  
\--------------------------------------------------------*/
#include <graphics_framework.h>
#include "NBodySystem.h"
#include "CameraSystem.h"

NBodySystem *sim = nullptr;
CameraSystem *cam = nullptr;

bool Initialise()
{
    cam = new CameraSystem();
    sim = new NBodySystem();
    return true;
}

bool Update(const float delta_time)
{  
    cam->ProcessInput(delta_time);
    sim->Update(delta_time);
    return true; 
}

bool Render(const float time)
{  
    cam->BeginPostProcess();
    sim->Render(cam->GetActiveCamera());
    cam->EndPostProcess(cam->GetActiveCamera());
    return true;
}

void CleanUp()
{
    delete sim;
    delete cam;
}

int main()
{
    // Initialise rendering framework
    graphics_framework::app application;
    // Link the framework to the simulation
    application.set_initialise(Initialise);
    application.set_update(Update);
    application.set_render(Render);
    application.run();
    CleanUp();

    return 0;
}