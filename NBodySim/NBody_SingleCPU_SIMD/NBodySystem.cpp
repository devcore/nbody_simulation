/*
File: NBodySystem.cpp
--------------------------------------------------------------------------------
For details see NBodySystem.h
*/
#include "NBodySystem.h"
#include "NBodyLayouts.h"
#include "InputHandler.h"

using namespace glm;
using namespace graphics_framework;

// Quick buffer size in bytes macro
#if SIMD_X4
#define BUFFER_SIZE m_nbodyBuffer.size()*sizeof(vec3)*4
#define NBODY_COUNT m_nbodyBuffer.size()*4
#else
#define BUFFER_SIZE m_nbodyBuffer.size()*sizeof(vec3)
#define NBODY_COUNT m_nbodyBuffer.size()
#endif
// Constants
const float g_smooth{ 0.8f };
const __m128 g_gravitation_x4 = _mm_set_ps1(6.674f * powf(10.0f, -11));
const __m128 g_smooth_x4 = _mm_set_ps1(0.8f);
const __m128 g_damping_x4 = _mm_set_ps1(0.99987f);

NBodySystem::NBodySystem()
{
    std::cerr << "NBODY: NBodySystem created." << std::endl;
    // Initialise default settings
    m_running = false;
    m_limit_test = false;
#if DEBUG | _DEBUG
    m_num_nbodies = 128;
#else
    #if SIMD_X4
        m_num_nbodies = 256/4;
    #else
        m_num_nbodies = 256;
    #endif
#endif
    m_nbodyBuffer.reserve(65536);
    // Load default cube distribution
    Layouts::SpiralGalaxy(m_nbodyBuffer, m_num_nbodies);
#if SIMD_X1
    std::vector<vec3> positions(m_nbodyBuffer.size());
    for (uint i = 0; i < m_nbodyBuffer.size(); ++i)
    {
        positions[i] = vec3(m_nbodyBuffer[i].pos.m128_f32[0],
                            m_nbodyBuffer[i].pos.m128_f32[1], 
                            m_nbodyBuffer[i].pos.m128_f32[2]);
    }
#elif SIMD_X4
    std::vector<vec3> positions(m_nbodyBuffer.size()*4);
    for (uint i = 0, n = 0; i < m_nbodyBuffer.size(); ++i)
    {
        for (uint j = 0; j < 4; ++j, ++n)
        {
            positions[n] = vec3(m_nbodyBuffer[i].px.m128_f32[j],
                                m_nbodyBuffer[i].py.m128_f32[j],
                                m_nbodyBuffer[i].pz.m128_f32[j]);
        }
    }
#endif
    // VBO and shader program
    m_quad.set_type(GL_POINTS);
    m_quad.add_buffer(positions, BUFFER_INDEXES::POSITION_BUFFER, GL_STREAM_DRAW);
    m_eff.add_shader("..\\Shaders\\billboard.vert", GL_VERTEX_SHADER);
    m_eff.add_shader("..\\Shaders\\billboard.geom", GL_GEOMETRY_SHADER);
    m_eff.add_shader("..\\Shaders\\billboard.frag", GL_FRAGMENT_SHADER);
    m_eff.build();
#if 0  // Performance test
    PerformanceTest();
#elif 0  // Unit test
    TestIntegration(1000);
#endif
    std::cerr << "NBODY: NBody count: " << NBODY_COUNT << std::endl;
}

NBodySystem::~NBodySystem()
{
    std::cerr << "NBODY: NBodySystem Destroyed." << std::endl;
    m_nbodyBuffer.clear();
}

void NBodySystem::ResetToNewLayout(const uint size,
                    void(*pfunc)(std::vector<NBody> &nbodies, const uint size))
{
    m_running = false; // Pause the simulation
    // Mark current VBO data as invalid
    glBindBuffer(GL_ARRAY_BUFFER, m_quad.get_buffer(POSITION_BUFFER));
    glBufferData(GL_ARRAY_BUFFER, BUFFER_SIZE, nullptr, GL_STREAM_DRAW);
    m_nbodyBuffer.clear();               
    pfunc(m_nbodyBuffer, size); // Layout NBodies in new pattern
    // Reallocate new buffer with the new size and update it with new positions
    glBufferData(GL_ARRAY_BUFFER, BUFFER_SIZE, nullptr, GL_STREAM_DRAW);
    vec3* ptr_mapbuffer = (vec3*)glMapBufferRange(GL_ARRAY_BUFFER, 0, BUFFER_SIZE, 
                                    GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT);
#if SIMD_X1
    for (uint i = 0; i < m_nbodyBuffer.size(); ++i)
    {
        ptr_mapbuffer[i] = vec3(m_nbodyBuffer[i].pos.m128_f32[0],
                                m_nbodyBuffer[i].pos.m128_f32[1], 
                                m_nbodyBuffer[i].pos.m128_f32[2]);
    }
    // Update geomtry vertex count
    m_quad.set_vertex_count(m_nbodyBuffer.size());
#elif SIMD_X4
    for (uint i = 0, n = 0; i < m_nbodyBuffer.size(); ++i)
    {
        for (uint j = 0; j < 4; ++j, ++n)
        {
            ptr_mapbuffer[n] = vec3(m_nbodyBuffer[i].px.m128_f32[j],
                                    m_nbodyBuffer[i].py.m128_f32[j],
                                    m_nbodyBuffer[i].pz.m128_f32[j]);
        }
    }
    // Update geomtry vertex count
    m_quad.set_vertex_count(m_nbodyBuffer.size()*4);
#endif
    glUnmapBuffer(GL_ARRAY_BUFFER);
}

void NBodySystem::Update(const float delta_time)
{
    // Change NBody layouts
    if (KeyPressOnce('1')) { ResetToNewLayout(m_num_nbodies, Layouts::SpiralGalaxy); }
    if (KeyPressOnce('2')) { ResetToNewLayout(m_num_nbodies, Layouts::TwinSpiralGalaxies); }
    if (KeyPressOnce('3')) { ResetToNewLayout(m_num_nbodies, Layouts::CubeDistribution); }
    if (KeyPressOnce('4')) { ResetToNewLayout(m_num_nbodies, Layouts::RandomDistribution); }
    if (KeyPressOnce('5')) { ResetToNewLayout(m_num_nbodies, Layouts::Singularity); }
    // Max NBody routine
    if (KeyPressOnce('6')) { m_limit_test = true; }
    if (KeyPressOnce('T')) { TestIntegration(100); }
    if (m_limit_test) { NBodyLimitTest(); }
    // Pause/Unpause simulation
    if (KeyPressOnce(GLFW_KEY_SPACE)) { m_running = !m_running; }
    if (m_running)
    { 
        Integrate(delta_time);
        StreamBuffer();
    }
}

#pragma optimize("", off)
void NBodySystem::Integrate(const float delta_time)
{
    // Use buffer size to not clash with limit test
    const uint nbody_count{ m_nbodyBuffer.size() };
    // Set x4 dealta_time
    const __m128 dt = _mm_set_ps1(delta_time);
#if SIMD_X1
    // Compute NBody attraction to each other. a = G*M_j*R_ij/(|R_ij|^2+E)^3/2
    for (uint i = 0; i < nbody_count; ++i)
    for (uint j = 0; j < nbody_count; ++j)
    {
        const NBody* jbody = &m_nbodyBuffer[j];
        // R_ij - Distance between bodies 
        __m128 dist = _mm_sub_ps(m_nbodyBuffer[i].pos, jbody->pos);
        // G*M_j*R_ij - Multiply Gravitational Constant, Mass, Distance 
        __m128 numerator = _mm_mul_ps(jbody->mass, 
                              _mm_mul_ps(g_gravitation_x4, dist));
        // Add Manitude and Smoothing length then raise to power 3/2
        __m128 denominator = _mm_set_ps1(pow(_mm_cvtss_f32(
                                _mm_dp_ps(dist, dist, 0x71))+g_smooth, 3.0f/2.0f));
        // Accumulate acceleration
        m_nbodyBuffer[i].accel = _mm_sub_ps(m_nbodyBuffer[i].accel, 
                                    _mm_div_ps(numerator, denominator));
    }
    // Eulerian semi-implicit integration
    for (uint i = 0; i < nbody_count; ++i)
    {
        NBody* nbody = &m_nbodyBuffer[i];
        nbody->vel = _mm_add_ps(nbody->vel, _mm_mul_ps(nbody->accel, dt));
        nbody->pos = _mm_add_ps(nbody->pos, _mm_mul_ps(nbody->vel, dt));
        nbody->accel = _mm_setzero_ps();
        nbody->vel = _mm_mul_ps(nbody->vel, g_damping_x4);
    }
#elif SIMD_X4
    // Compute NBody attraction to each other. a = G*M_j*R_ij/(|R_ij|^2+E)^3/2
    for (uint i = 0; i < nbody_count; ++i)
    for (uint j = 0; j < nbody_count; ++j)
    {
        const NBody* jbody = &m_nbodyBuffer[j];
        /* Compute attraction of four bodies at once:
           i_body x1 x2 x3 x4 - j_body[0] x1 x1 x1 x1
                                j_body[1] x2 x2 x2 x2
                                j_body[2] x3 x3 x3 x3
                                j_body[3] x4 x4 x4 x4
        */
        for (uint k = 0; k < 4; ++k)
        {
            // R_ij - Distance between bodies  
            __m128 distX = _mm_sub_ps(m_nbodyBuffer[i].px, _mm_set_ps1(jbody->px.m128_f32[k]));
            __m128 distY = _mm_sub_ps(m_nbodyBuffer[i].py, _mm_set_ps1(jbody->py.m128_f32[k]));
            __m128 distZ = _mm_sub_ps(m_nbodyBuffer[i].pz, _mm_set_ps1(jbody->pz.m128_f32[k]));
            // G*M_j*R_ij - Gravitational Constant Mass Distance 
            __m128 numeratorX = _mm_mul_ps(_mm_set_ps1(jbody->mass.m128_f32[k]),
                                   _mm_mul_ps(g_gravitation_x4, distX));
            __m128 numeratorY = _mm_mul_ps(_mm_set_ps1(jbody->mass.m128_f32[k]),
                                   _mm_mul_ps(g_gravitation_x4, distY));
            __m128 numeratorZ = _mm_mul_ps(_mm_set_ps1(jbody->mass.m128_f32[k]),
                                   _mm_mul_ps(g_gravitation_x4, distZ));
            // | R_ij | - Magnitude
            __m128 length = _mm_add_ps(_mm_add_ps(_mm_mul_ps(distX, distX), 
                                                  _mm_mul_ps(distY, distY)),
                                                  _mm_mul_ps(distZ, distZ));
            // Add Manitude and Smoothing length the take the sqrt
            __m128 denominator = _mm_sqrt_ps(_mm_add_ps(length, g_smooth_x4));
            // Cube the result
            denominator = _mm_mul_ps(denominator,
                             _mm_mul_ps(denominator, denominator));
            // Accumulate acceleration
            m_nbodyBuffer[i].ax = _mm_sub_ps(m_nbodyBuffer[i].ax, 
                                     _mm_div_ps(numeratorX, denominator));
            m_nbodyBuffer[i].ay = _mm_sub_ps(m_nbodyBuffer[i].ay,
                                     _mm_div_ps(numeratorY, denominator));
            m_nbodyBuffer[i].az = _mm_sub_ps(m_nbodyBuffer[i].az,
                                     _mm_div_ps(numeratorZ, denominator));
        }
    }
    // Eulerian semi-implicit integration
    for (uint i = 0; i < nbody_count; ++i)
    {
        // Velocities XYZ + Accelerations XYZ * delta time
        m_nbodyBuffer[i].vx = _mm_add_ps(m_nbodyBuffer[i].vx,
                                 _mm_mul_ps(m_nbodyBuffer[i].ax, dt));
        m_nbodyBuffer[i].vy = _mm_add_ps(m_nbodyBuffer[i].vy,
                                 _mm_mul_ps(m_nbodyBuffer[i].ay, dt));
        m_nbodyBuffer[i].vz = _mm_add_ps(m_nbodyBuffer[i].vz,
                                 _mm_mul_ps(m_nbodyBuffer[i].az, dt));
        // Positions XYZ + Velocities XYZ * delta time
        m_nbodyBuffer[i].px = _mm_add_ps(m_nbodyBuffer[i].px, 
                                 _mm_mul_ps(m_nbodyBuffer[i].vx, dt));
        m_nbodyBuffer[i].py = _mm_add_ps(m_nbodyBuffer[i].py,
                                 _mm_mul_ps(m_nbodyBuffer[i].vy, dt));
        m_nbodyBuffer[i].pz = _mm_add_ps(m_nbodyBuffer[i].pz,
                                 _mm_mul_ps(m_nbodyBuffer[i].vz, dt));
        // Accelerations XYZ reset
        m_nbodyBuffer[i].ax = _mm_setzero_ps();
        m_nbodyBuffer[i].ay = _mm_setzero_ps();
        m_nbodyBuffer[i].az = _mm_setzero_ps();
        // Velocities XYZ Damping
        m_nbodyBuffer[i].vx = _mm_mul_ps(m_nbodyBuffer[i].vx, g_damping_x4);
        m_nbodyBuffer[i].vy = _mm_mul_ps(m_nbodyBuffer[i].vy, g_damping_x4);
        m_nbodyBuffer[i].vz = _mm_mul_ps(m_nbodyBuffer[i].vz, g_damping_x4);
    }
#endif
}
#pragma optimize("", on)

void NBodySystem::StreamBuffer()
{
    // Stream new positions
    glBindBuffer(GL_ARRAY_BUFFER, m_quad.get_buffer(POSITION_BUFFER));
    // GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT | GL_MAP_INVALIDATE_RANGE_BIT
    vec3* ptr_mapbuffer = (vec3*)glMapBufferRange(GL_ARRAY_BUFFER, 0, BUFFER_SIZE,
                                 GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT);
#if SIMD_X1
    for (uint i = 0; i < m_nbodyBuffer.size(); ++i)
    {
        ptr_mapbuffer[i] = vec3(m_nbodyBuffer[i].pos.m128_f32[0],
                                m_nbodyBuffer[i].pos.m128_f32[1], 
                                m_nbodyBuffer[i].pos.m128_f32[2]);
    }
#elif SIMD_X4
    for (uint i = 0, n = 0; i < m_nbodyBuffer.size(); ++i)
    {
        for (uint j = 0; j < 4; ++j, ++n)
        {
            ptr_mapbuffer[n] = vec3(m_nbodyBuffer[i].px.m128_f32[j],
                                    m_nbodyBuffer[i].py.m128_f32[j],
                                    m_nbodyBuffer[i].pz.m128_f32[j]);
        }
    }
#endif
    glUnmapBuffer(GL_ARRAY_BUFFER);
}

void NBodySystem::Render(const camera *cam)
{
    // Render a quad billboard for each nbody point
    renderer::bind(m_eff);
    mat4 V = cam->get_view();
    mat4 P = cam->get_projection();
    const vec3 cam_pos = cam->get_position();
    glUniformMatrix4fv(m_eff.get_uniform_location("V"), 1, GL_FALSE, value_ptr(V));
    glUniformMatrix4fv(m_eff.get_uniform_location("P"), 1, GL_FALSE, value_ptr(P));
    glUniform3fv(m_eff.get_uniform_location("camera_pos"), 1, value_ptr(cam_pos));
    renderer::render(m_quad);
}

void NBodySystem::NBodyLimitTest()
{
    using namespace std::chrono;
    // Create timing functions
    auto start = high_resolution_clock::now();
    auto end = high_resolution_clock::now();
    float delta_time = 0.0f;
    const uint iter_count = 5;
    const float target_FPS = iter_count * 0.016f;
    // Loop while the frame time is ~60 times a second
    for (uint n = 0; n < iter_count && delta_time < target_FPS; ++n)
    {
        // Push back 4 new NBodies
        if (n != 0)
        {
#if SIMD_X4
            vec3 pos[4] = { vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f) };
            float mass[4] = { 10000.0f, 10000.0f, 10000.0f, 10000.0f };
            m_nbodyBuffer.push_back(NBody(pos, mass));
#else
            m_nbodyBuffer.push_back(NBody(vec3(0.0f), 1000000.0f));
            m_nbodyBuffer.push_back(NBody(vec3(0.0f), 1000000.0f));
            m_nbodyBuffer.push_back(NBody(vec3(0.0f), 1000000.0f));
            m_nbodyBuffer.push_back(NBody(vec3(0.0f), 1000000.0f));
#endif
        }
        // Start time stamp
        start = high_resolution_clock::now();
        for (uint i = 0; i < iter_count; ++i)
        {
            Integrate(delta_time);
        }
        // Compute time taken
        end = high_resolution_clock::now();
        delta_time = (float)(duration_cast<microseconds>(end-start).count());
        delta_time *= 0.000001f;
    }
    // If the exit was due to performance, cancel futher tests
    if (delta_time > target_FPS)
    {
        m_limit_test = false;
        m_num_nbodies = m_nbodyBuffer.size();
    }
    ResetToNewLayout(m_nbodyBuffer.size(), Layouts::CubeDistribution);
    std::cerr << "NBODY: New count: " << NBODY_COUNT << std::endl;
}

void NBodySystem::PerformanceTest()
{
    using namespace std::chrono;
    // Pre-create variables used for timing and setup the system
    auto start = high_resolution_clock::now();
    auto end = high_resolution_clock::now();
    double total{ 0.0 };
    const uint iter_count = 10;
#if SIMD_X4
    const uint nbody_count[4]{ 256, 512, 1024, 2048 };
#else
    const uint nbody_count[4]{ 1024, 2048, 4096, 8192 };
#endif
    // Create a file to store the data
    std::ofstream data_file("NBody_performance_x4.csv", std::ofstream::out);
    data_file << "NBodies, 1024, 2048, 4096, 8192\n";
    data_file << "Test_Name, ";
    // Test with 4 different sizes for 1000 iterations
    for (uint n = 0; n < 4; ++n)
    {
        Layouts::SpiralGalaxy(m_nbodyBuffer, nbody_count[n]);
        start = high_resolution_clock::now();
        for (uint i = 0; i < iter_count; ++i)
        {
            Integrate(0.02f);
        }
        end = high_resolution_clock::now();
        total = static_cast<double>(duration_cast<microseconds>(end - start).count());
        total /= iter_count; // Compute average
        total *= 0.001; // Convert to milliseconds
        data_file << total << ", ";
        std::cerr << n << ": " << total << std::endl;
    }
    data_file.close();
}

void NBodySystem::TestIntegration(uint iter)
{
    using namespace std;
    // Space out 8 nbodies evenly
#if SIMD_X4
    m_num_nbodies = 2; // X4
    ResetToNewLayout(m_num_nbodies, Layouts::TestLayout);
    // Integrate for N times
    for (uint i = 0; i < iter; ++i)
    {
        Integrate(0.01f);
    }
    // Store the new positions in a file
    string file_name = "SINGLE_CPU_SIMDX4_IntegrationTest_"+to_string(iter)+string(".csv");
    ofstream data_file(file_name, ofstream::out);
    data_file << "Positions after: " << iter << "\n";
    data_file << "x,y,z" << "\n";
    for (uint i = 0; i < m_num_nbodies; i++)
    {
        for (uint j = 0; j < 4; j++)
        {
            data_file << m_nbodyBuffer[i].px.m128_f32[i] << ",";
            data_file << m_nbodyBuffer[i].py.m128_f32[i] << ",";
            data_file << m_nbodyBuffer[i].pz.m128_f32[i] << ",";
            data_file << "\n";
        }
    }
#else
    m_num_nbodies = 8;
    ResetToNewLayout(m_num_nbodies, Layouts::TestLayout);
    // Integrate for N times
    for (uint i = 0; i < iter; ++i)
    {
        Integrate(0.01f);
    }
    // Store the new positions in a file
    string file_name = "SINGLE_CPU_SIMDX1_IntegrationTest_"+to_string(iter)+string(".csv");
    ofstream data_file(file_name, ofstream::out);
    data_file << "Positions after: " << iter << "\n";
    data_file << "x,y,z" << "\n";
    for (uint i = 0; i < m_num_nbodies; i++)
    {
        data_file << m_nbodyBuffer[i].pos.m128_f32[0] << ",";
        data_file << m_nbodyBuffer[i].pos.m128_f32[1] << ",";
        data_file << m_nbodyBuffer[i].pos.m128_f32[2] << ",";
        data_file << "\n";
    }
#endif
    data_file.close();
    StreamBuffer();
}