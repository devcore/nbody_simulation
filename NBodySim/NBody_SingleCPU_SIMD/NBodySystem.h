/*--------------------------------------------------------\
| File: NBodySystem.h                                     |
|x-------------------------------------------------------x|
| Details: Creates a number of NBodies that attract each  |
| other with O(n^2) computational complexity. Attraction  |
| is calculated using: a = G*M_j*R_ij / (|R_ij|^2+E)^3/2  |
| Where: a - acceleration, G - gravitational constant,    |
| M - mass, R - distance and E - smoothing factor.        |
| Formula from: NVidia GPU gems 3 NBody Simulation        |
|x-------------------------------------------------------x|
| SIMD_X1:                                                |
| Uses 3 lanes of an register and discard the last value. |
| SIMD_X4:                                                |
| Adapts NBody data structure to tightly pack 4 NBodies   |
| and operates all four at once.                          |
| VERSION CHANGE:                                         |
| Change either #defined value below to 1 and the other 0 |
|x-------------------------------------------------------x|
| Author:                              Andrius Jefremovas |
| Description:    Simulates NBody attraction between them.|
\--------------------------------------------------------*/
#ifndef NBODY_SYSTEM_H
#define NBODY_SYSTEM_H

#include <graphics_framework.h>

#define SIMD_X1 0
#define SIMD_X4 1

// The defination of a single NBody
#if SIMD_X1
__declspec(align(16)) struct NBody
{
    __m128 mass;  // Mass         | 16 |
    __m128 pos;   // Position     | 32 |
    __m128 vel;   // Velocity     | 48 |
    __m128 accel; // Acceleration | 64 |
    // Default constructor
    NBody() : 
        pos(_mm_set_ps1(0.0f)), 
        vel(_mm_set_ps1(0.0f)),
        accel(_mm_set_ps1(0.0f)), 
        mass(_mm_set_ps1(0.0f)) { }
    // Creates stationary NBody: position, mass.
    NBody(const glm::vec3 &pos, const float &mass) :
        pos(_mm_set_ps(0.0f, pos.z, pos.y, pos.x)),
        vel(_mm_set_ps1(0.0f)),
        accel(_mm_set_ps1(0.0f)),
        mass(_mm_set_ps1(mass)) { }
    // Creates moving NBody: position, velocity, mass.
    NBody(const glm::vec3 &pos, const glm::vec3 &vel, const float &mass) :
        pos(_mm_set_ps(0.0f, pos.z, pos.y, pos.x)),
        vel(_mm_set_ps(0.0f, vel.z, vel.y, vel.x)),
        accel(_mm_set_ps1(0.0f)), 
        mass(_mm_set_ps1(mass)) { }
};
// Definition of 4 nbodies using SIMD ideal data structure
#elif SIMD_X4
__declspec(align(16)) struct NBody
{
    __m128 mass;         // Mass         |  16 |
    __m128 px, py, pz;   // Position     |  64 |
    __m128 vx, vy, vz;   // Velocity     | 112 |
    __m128 ax, ay, az;   // Acceleration | 160 |
    // Default constructor
    NBody() :
        px(_mm_setzero_ps()), py(px), pz(px),
        vx(_mm_setzero_ps()), vy(vx), vz(vx),
        ax(_mm_setzero_ps()), ay(ax), az(ax),
        mass(_mm_setzero_ps()) { }
    // Creates stationary NBody: position, mass.
    NBody(const glm::vec3 pos[4], const float mass[4]) :
        px(_mm_set_ps(pos[3].x, pos[2].x, pos[1].x, pos[0].x)),
        py(_mm_set_ps(pos[3].y, pos[2].y, pos[1].y, pos[0].y)),
        pz(_mm_set_ps(pos[3].z, pos[2].z, pos[1].z, pos[0].z)),
        vx(_mm_setzero_ps()), vy(vx), vz(vx),
        ax(_mm_setzero_ps()), ay(ax), az(ax),
        mass(_mm_set_ps(mass[3], mass[2], mass[1], mass[0])) { }
    // Creates moving NBody: position, velocity, mass.
    NBody(const glm::vec3 pos[4], const glm::vec3 vel[4], const float mass[4]) :
        px(_mm_set_ps(pos[3].x, pos[2].x, pos[1].x, pos[0].x)),
        py(_mm_set_ps(pos[3].y, pos[2].y, pos[1].y, pos[0].y)),
        pz(_mm_set_ps(pos[3].z, pos[2].z, pos[1].z, pos[0].z)),
        vx(_mm_set_ps(vel[3].x, vel[2].x, vel[1].x, vel[0].x)),
        vy(_mm_set_ps(vel[3].y, vel[2].y, vel[1].y, vel[0].y)),
        vz(_mm_set_ps(vel[3].z, vel[2].z, vel[1].z, vel[0].z)),
        ax(_mm_setzero_ps()), ay(ax), az(ax),
        mass(_mm_set_ps(mass[3], mass[2], mass[1], mass[0])) { }
};
#endif

struct NBodySystem
{
    NBodySystem();
    ~NBodySystem();
    // Resets nbodies to new locations and resizes OpenGL buffer
    void ResetToNewLayout(const glm::uint size,
            void(*pFunc)(std::vector<NBody> &nbodies, const glm::uint size));
    // Takes input to control simulation and calls integrate/streambuffer
    void Update(const float delta_time);
    // Renders each NBody using the buffer of positions to create quads
    void Render(const graphics_framework::camera *cam);
private:
    // Performs NBody attraction and new position calculations
    void Integrate(const float delta_time);
    // Streams NBody positions to a OpenGL buffer
    void StreamBuffer();
    // Test how many NBodies can be simulated withing 60 fps
    void NBodyLimitTest();
    // Test iteration timings of various numbers of nbodies
    void PerformanceTest();
    // Performs integration for N iterations and print the results to a file
    void TestIntegration(const glm::uint iter);
    // System properties
    bool m_running;
    bool m_limit_test;
    glm::uint m_num_nbodies;
    std::vector<NBody> m_nbodyBuffer;
    // NBody display properties
    graphics_framework::effect m_eff;
    graphics_framework::geometry m_quad;
};

#endif /* !NBODY_SYSTEM_H */