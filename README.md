## **REQUIREMENTS** ##
Microsoft Visual Studio 2012 or newer.
## **SUMMARY** ##
An implementation of the [NBody Problem](https://en.wikipedia.org/wiki/N-body_problem).
The goal is to increase the performance of this application using hardware acceleration, then measure and compare it.
NOTE: Optimization is turned off only for the integration function block to avoid skewed results from auto thread and SIMD optimization. 

Features 5 different version (poor naming): 
1. Single CPU      - base version that uses 1 cpu core.
2. Single CPU SIMD - uses 128-bit SSE registers to perform 4 wide vector operation (two variants array of structs AoS and struct of arrays SoA).
3. Multi CPU       - uses multiple CPU cores. Three variants: C++ 11 threads, OpenMP and PPL see headers of details.
4. Multi CPU SIMD  - uses parallel processing combined with SIMD 4 wide vector operations.
5. GPU             - uses OpenGL compute shaders. 
### **NOTICE** ###
Copyright 2015 Andrius Jefremovas

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.